# Algo Manager (_algoman_)
![alt text](https://img.shields.io/badge/version-4.2.0-green.svg "version 4.2.0")

The _algoman_ library provides capabilities for calculating air quality over a grid.

Some of the provided functionality includes:

- IDW-based interpolation methods for sparse station data
- Smoothing interpolations for model data (i.e CAMS)
- Overlaying and combining multiple data layers

## This version contains the BOSCH KLUDGE
**Kludge**: _a not so elegant solution to a problem that is done quickly._
While under testing by bosch the accuracy of BreezoMeter results came
into question, the problem came from stations that in reality were
effected by traffic but were not effected by traffic fixes causing
massive spike in certain grid points.
The fix implemented **until a fix can be applied to the traffic itself**
is that for every station we will replace the nearest grid point with the
values of the maximum station (in case there is more then one).
The part that does the replacing is implemented here while the part arranges
the data is in the algorithm itself.

## Requirements
- Python >= 3.6
- NumPy >= 1.17.2
- Pandas >= 0.25.1
- SciPy >= 1.3.1

Note that newer versions of each library might still work, but are currently untested.


## Installation
_algoman_ can be installed with _pip_ directly from the repository with the following commands, replacing `{branch_or_tag}` with the wanted branch name (i.e `staging`) or tagged version (i.e `1.0.0`).

```shell
# SSH
pip install hg+ssh://hg@bitbucket.org/breezometer/algo-manager@{branch_or_tag}#egg=algoman

# HTTPS
pip install hg+https://bitbucket.org/breezometer/algo-manager@{branch_or_tag}#egg=algoman
```


## Usage
The library exposes a single main class, `Manager`, which provides the entry point for running the whole suite of calculations for a given tile. See inline documentation for more details on how to use the class.


## Development
Contributions are welcome in the standard procedure: through pull-requests approved by at least one algo engineer and one progrmmer/software engineer.

Please consider adding tests to parts you add/edit.


## Tests
We recommend using pytest to run the tests, for example:

```shell
pytest tests/tests.py
```


Note: As adding tests is WIP and the coverage is still very low (:scream:), there are no strict requirements other than
>please make sure you don't break anything kthxbye
