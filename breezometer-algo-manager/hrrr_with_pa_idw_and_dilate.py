import numpy as np
import pandas as pd
import json
from datetime import datetime as dt

import numpy as np
import pandas as pd
import cv2
from scipy.interpolate import interpn

import matplotlib.pyplot as plt
plt.rcParams.update({'font.size': 14})

from algoman.algorithms import IDW
from algoman.conversion import d2r
from algoman.layer_weighters import station_distance_weighter
from algoman.stacker import smoosh_frames

def load_files(hrrr_path, pa_nearest_gp_path):
    '''
    load and prep data.

    hrrr_path: str
        full path and file name of the hrrr file from algo input
    pa_nearest_gp_path : str
        full path and file name of the pa data, after adding the nearest hrrr gp coordinates
    '''
    # load hrrr
    j = json.load(open(f'{hrrr_path}'))
    df_hrrr_data = pd.DataFrame(j['values'])
    hrrr_metadata_dict = j['metadata'] # to be used before rapping back to json
    original_hrrr_cols = df_hrrr_data.columns
    df_hrrr_data['hrrr_pm25'] = pd.json_normalize(df_hrrr_data['pollutants'])
    df_hrrr_data = df_hrrr_data.drop(columns='pollutants')

    # load pa
    df_nearest_gp = pd.read_pickle(f'{pa_nearest_gp_path}')
    df_nearest_gp = df_nearest_gp[['lat', 'lon', 'pm25', 'hrrr_lat', 'hrrr_lon']].copy()
    df_nearest_gp = df_nearest_gp.rename(columns={'lat': 'pa_lat', 'lon': 'pa_lon'})

    return df_hrrr_data, df_nearest_gp, hrrr_metadata_dict, original_hrrr_cols


def _get_kernel_sizes(ksize_smallest, ksize_largest, erode_n_times):
    """
    Calculate the range of kernel sizes to use for erosion.

    We transition from a large ksize (e.g. ``ksize_largest=10``) at the edges
    to a smaller one (e.g. ``ksize_smallest=3``) farther from the edges. This
    provides a smoother and more moderate gradient.

    Returns an array of integers of length ``erode_n_times``, which are the
    kernel sizes to use.

    Notes
    -----
    The sequence of kernel-sizes is built as a composition of two parts:
    (a) contains the trasition from largest kernel to smallest (25% of length);
    (b) contains the "tail" of small kernels (75% of length).

    For example, the inputs::

        erode_n_times = 20
        ksize_smallest = 3
        ksize_largest = 10

    Will result in these parts::

        large_sequence = [10, 8, 6, 4, 3]
        small_sequence = [3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3]

    Which form the final kernel-sizes sequence::

        [10, 8, 6, 4, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3]
    """
    n_kernels_large_to_small = int(erode_n_times * 0.25)
    n_kernels_small = int(erode_n_times * 0.75) + 1  # +1 because of the int

    # Transition from large to small ksizes, with n_large_size as the number
    # of steps
    large_sequence = np.linspace(start=ksize_largest,
                                 stop=ksize_smallest,
                                 num=n_kernels_large_to_small,
                                 dtype=int)
    # Add n_small_size times of the small ksize
    small_sequence = ksize_smallest * np.ones(n_kernels_small, dtype=int)

    return np.concatenate([large_sequence, small_sequence], axis=0)


def calc_pa_average(pa_nearest_gp_data_df, method='median'):
    '''
    group all PA sensors that are nearest to the same hrrr grid point.
    method - the averaging method to aggregat the PA values, defult is median.
    '''
    nearest_grouped = pa_nearest_gp_data_df.groupby(by=['hrrr_lat','hrrr_lon'], sort=False, dropna=False, as_index=False)
    nearest_grouped = nearest_grouped.agg(
        mean_pa_lat=('pa_lat','mean'),
        mean_pa_lon=('pa_lon','mean'),
        averaged_pa_value=('pm25', method),
        pa_count=('pm25','size')
        )

    return nearest_grouped


def override_hrrr_with_pa_average(hrrr_data_df, pa_nearest_gp_grouped):
    '''
    overide all hrrr gp that have PA aggregated values
    '''

    merged = pd.merge(left=hrrr_data_df,
                      right=pa_nearest_gp_grouped,
                      how='left',
                      left_on=['lat','lon'],
                      right_on=['hrrr_lat','hrrr_lon'])

    merged.loc[merged.pa_count.isna(), 'pa_count'] = 0
    # replace pm2.5 value
    mask = merged.pa_count > 1
    replace_idx = mask[mask].index

    new_hrrr_df = merged.copy()
    new_hrrr_df.loc[replace_idx, 'hrrr_pm25'] = new_hrrr_df.loc[replace_idx, 'averaged_pa_value']
    new_hrrr_df = new_hrrr_df[['lat', 'lon', 'hrrr_pm25']]

    return new_hrrr_df, merged #merged for statistics


def prep_new_hrrr_to_json(new_hrrr_df, hrrr_metadata, original_hrrr_cols):
    new_hrrr_df = new_hrrr_df[['lat', 'lon', 'hrrr_pm25']]
    new_hrrr_df['pollutants'] = new_hrrr_df['hrrr_pm25'].apply(lambda x: {'pm25': x})
    new_hrrr_df = new_hrrr_df.drop(columns='hrrr_pm25')

    new_hrrr_df = new_hrrr_df[original_hrrr_cols]

    new_hrrr_list_of_dicts = new_hrrr_df.to_dict('records')

    new_hrrr_dict_full = {'values': new_hrrr_list_of_dicts,
                          'metadata': hrrr_metadata}

    return new_hrrr_dict_full


def visualize_stats(merged_df, hrrr_fn):

    # plot general distribution of affected gp
    merged_df['pa_count'] = merged_df['pa_count'].fillna(0)

    fig = plt.figure(figsize=(19,9))

    bins = list(range(int(merged_df['pa_count'].min()), int(merged_df['pa_count'].max())))

    plt.hist(merged_df['pa_count'], bins=bins, log=True)
    plt.title(f'HRRR grid points distribution by the number of affecting PA sensors\n {hrrr_fn}')
    plt.ylabel('# grid points (Note the log scale!)')
    plt.xlabel('number of PA sensors affecting an HRRR gridpoint')
    plt.grid()

    # plot distribution of affected gp by type of effect
    affected_gp = merged_df.copy().dropna().reset_index(drop=True)
    hrrr_higher_than_pa = affected_gp[affected_gp.hrrr_pm25 > affected_gp.averaged_pa_value].copy().reset_index(drop=True)
    hrrr_lower_than_pa = affected_gp[affected_gp.hrrr_pm25 < affected_gp.averaged_pa_value].copy().reset_index(drop=True)
    hrrr_equal_to_pa = affected_gp[affected_gp.hrrr_pm25 == affected_gp.averaged_pa_value].copy().reset_index(drop=True)

    affected_data = [hrrr_higher_than_pa.pa_count,
                     hrrr_equal_to_pa.pa_count,
                     hrrr_lower_than_pa.pa_count]

    labels = [f'hrrr > pa : {np.round(100*len(hrrr_higher_than_pa)/len(affected_gp),1)}% of affectd gp',
              f'hrrr == pa : {np.round(100*len(hrrr_equal_to_pa)/len(affected_gp),1)}% of affectd gp',
              f'hrrr < pa : {np.round(100*len(hrrr_lower_than_pa)/len(affected_gp),1)}% of affectd gp']

    fig = plt.figure(figsize=(19,9))

    bins = 20

    plt.hist(affected_data, bins=bins, align='mid', log=True, label=labels)
    plt.title('Affected HRRR grid points distribution by the number of affecting PA sensors\n groupd by type of change\n {hrrr_fn}')
    plt.ylabel('# *affected* grid points (Note the log scale!)')
    plt.xlabel('number of PA sensors affecting an HRRR gridpoint')
    plt.grid()
    plt.legend()

    return

if __name__=='__main__':

    data_folder_path = 'C:\\Users\\Anne\\Documents\\July2021\\APPLE-task-FORCE\\patch_PA_to_hrrr\\data\\'
    results_folder_path = 'C:\\Users\\Anne\\Documents\\July2021\\APPLE-task-FORCE\\patch_PA_to_hrrr\\fixed_hrrr\\'

    test_files = {'hr1': ['2021-08-06T00_00_00_hrrr.json', '2021-08-06T00-00_pa_with_hrrr.pkl'],
                  'hr2': ['2021-08-06T12_00_00_hrrr.json', '2021-08-06T12-00_pa_with_hrrr.pkl']}

    for hr in list(test_files.keys()):
        hrrr_fn = f'{data_folder_path}{test_files[hr][0]}'
        nearest_gp_fn = f'{data_folder_path}{test_files[hr][1]}'

        print(f'starting {hr}')
        print(test_files[hr][0])
        print(f'{dt.utcnow()} - loading files...')
        hrrr_data, pa_nearest_gp_data, hrrr_metadata, original_hrrr_cols = load_files(hrrr_fn, nearest_gp_fn)

        print(f'{dt.utcnow()} - aggregate PA...')
        pa_nearest_gp_grouped = calc_pa_average(pa_nearest_gp_data, method='median')

        # for IDW
        grid_hrrr = hrrr_data[['lat','lon']].copy()

        pa_stations_groupd = pa_nearest_gp_grouped[pa_nearest_gp_grouped.pa_count>1].copy()
        pa_stations_groupd = pa_stations_groupd[['hrrr_lat', 'hrrr_lon', 'averaged_pa_value']]
        pa_stations_groupd = pa_stations_groupd.rename(columns={'hrrr_lat': 'lat',
                                                                'hrrr_lon': 'lon',
                                                                'averaged_pa_value': 'pm25'})

        max_dist_deg = 2.5 * 0.027    # 2.5 pixels
        max_dist_r = d2r(max_dist_deg)

        # pivoted_hrrr = hrrr_data.pivot_table(index='lat', columns='lon', values='hrrr_pm25')
        california_bbox = {'SW': {'lat': 32.385, 'lon': -124.545},
                           'NE': {'lat': 42.108, 'lon': -115.954}}

        california_hrrr = grid_hrrr[(grid_hrrr.lat > california_bbox['SW']['lat']) &
                                    (grid_hrrr.lat < california_bbox['NE']['lat']) &
                                    (grid_hrrr.lon > california_bbox['SW']['lon']) &
                                    (grid_hrrr.lon < california_bbox['NE']['lon']) ].copy().reset_index(drop=True)

        california_pa = pa_stations_groupd[(pa_stations_groupd.lat > california_bbox['SW']['lat']) &
                                           (pa_stations_groupd.lat < california_bbox['NE']['lat']) &
                                           (pa_stations_groupd.lon > california_bbox['SW']['lon']) &
                                           (pa_stations_groupd.lon < california_bbox['NE']['lon']) ].copy().reset_index(drop=True)

        idw_pa_grouped = IDW(california_hrrr, california_pa, max_dist_r)

        min_dist_r = d2r(0.027) # one pixel

        weights = station_distance_weighter(california_hrrr, california_pa, min_dist_r, max_dist_r)

        california_hrrr_with_data = hrrr_data.copy().rename(columns={'hrrr_pm25': 'pm25'})
        california_hrrr_with_data = california_hrrr_with_data[
            (california_hrrr_with_data.lat > california_bbox['SW']['lat']) &
            (california_hrrr_with_data.lat < california_bbox['NE']['lat']) &
            (california_hrrr_with_data.lon > california_bbox['SW']['lon']) &
            (california_hrrr_with_data.lon < california_bbox['NE']['lon']) ].copy().reset_index(drop=True)

        california_circles_override = smoosh_frames(idw_pa_grouped, california_hrrr_with_data, weights)
        hrrr = hrrr_data.copy()
        hrrr.rename(columns ={'hrrr_pm25': 'pm25'}, inplace=True)
        hrrr.loc[(hrrr.lat > california_bbox['SW']['lat']) &
                 (hrrr.lat < california_bbox['NE']['lat']) &
                 (hrrr.lon > california_bbox['SW']['lon']) &
                 (hrrr.lon < california_bbox['NE']['lon'])] = california_circles_override
        final_pivoted = california_circles_override.pivot_table(index='lat', columns='lon', values='pm25')
        # final_r = hrrr.rename(columns={'pm25': 'hrrr_pm25'})
        # new_hrrr_dict_full = prep_new_hrrr_to_json(new_hrrr_df=final_r, hrrr_metadata=hrrr_metadata, original_hrrr_cols=original_hrrr_cols)
        # with open(f'{results_folder_path}FIXED_{test_files[hr][0]}', 'w') as fout:
        #     json.dump(new_hrrr_dict_full, fout)


        #todo: make a mask of pa_on_grid

        pa_nearest_gp_grouped.rename(columns={'hrrr_lat': 'lat', 'hrrr_lon': 'lon', 'averaged_pa_value': 'pm25'}, inplace=True)
        pa_raw = hrrr_data.merge(pa_nearest_gp_grouped, on=['lat', 'lon'], how='left')
        # pa_raw.loc[pa_raw.pa_count.isna(), 'pa_count'] = 0
        # replace pm2.5 value
        # masking = pa_raw.pa_count > 1
        # replace_idx = masking[masking].index
        # pa_raw = pa_raw.loc[replace_idx]
        california_pa = pa_raw[(pa_raw.lat > california_bbox['SW']['lat']) &
                               (pa_raw.lat < california_bbox['NE']['lat']) &
                               (pa_raw.lon > california_bbox['SW']['lon']) &
                               (pa_raw.lon < california_bbox['NE']['lon'])].copy().reset_index(
            drop=True)


        pa = california_pa[['lat', 'lon', 'pm25']]
        # todo:
        pa = pa.pivot_table(index='lat', columns='lon', values='pm25')
        mask = np.ones(pa.size)
        mask[np.isnan(pa.values.flatten())] = 0

        # edges and contour (iterable)

        pa_edges = cv2.Canny(mask.copy().astype('uint8').reshape(pa.shape), 0.0, 0.5) / 255
        # todo: fix or remove the next couple of lines
        # pa_edges = pa_edges.loc[pa_edges.astype(bool).flatten(), ['lat', 'lon']].reset_index(drop=True)
        # (threshold, pa_edges) = cv2.threshold(pa_edges, 122, 255, 0)
        pa_edges = pa_edges.astype(np.uint8)
        contours, hierarchy = cv2.findContours(pa_edges, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

        extent = (california_bbox['SW']['lon'],
                  california_bbox['NE']['lon'],
                  california_bbox['SW']['lat'],
                  california_bbox['NE']['lat'])     # (left, right, bottom, top)

        # plt.figure()
        # plt.imshow(final_pivoted, interpolation=None, origin='lower', extent=extent,
        #            vmin=0, vmax=100)
        # plt.colorbar()

        kernel = np.array([[0.5, 0.5, 0.5, 0.5, 0.5],
                           [0.5, 0.8, 0.8, 0.8, 0.5],
                           [0.5, 0.8, 1, 0.8, 0.5],
                           [0.5, 0.8, 0.8, 0.8, 0.5],
                           [0.5, 0.5, 0.5, 0.5, 0.5]])

        for cont in contours:
            dilated_mask = cv2.dilate(src=mask, kernel=kernel, iterations=1)

        # calculate smooshing weights,
        weights = dilated_mask.copy().astype('uint8').reshape(pa.shape)
        # todo: add PA indexes somehow
        # weights = weights.pivot_table(index='lat', columns='lon', values='pm25')
        # todo: make sure this smooshing works. idw_pivoted(269, 275); california_hrrr_with_data (360, 318)
        idw_pivoted = idw_pa_grouped.pivot_table(index='lat', columns='lon', values='pm25')
        california_hrrr_with_data = california_hrrr_with_data.pivot_table(index='lat', columns='lon', values='pm25')
        # todo fix error  KeyError: "['lat' 'lon'] not found in axis"
        final_pivoted = smoosh_frames(idw_pivoted, california_hrrr_with_data, dilated_mask)
        # unpivot:
        result = final_pivoted.melt(id_vars=['lat'], var_name='lon', value_name='pm25')

        # add to hrrr json:
        hrrr = hrrr_data.copy()
        hrrr.rename(columns={'hrrr_pm25': 'pm25'}, inplace=True)

        # todo: fix value replacement
        hrrr.loc[(hrrr.lat > california_bbox['SW']['lat']) &
                 (hrrr.lat < california_bbox['NE']['lat']) &
                 (hrrr.lon > california_bbox['SW']['lon']) &
                 (hrrr.lon < california_bbox['NE']['lon'])] = result

        hrrr_pa = hrrr.rename(columns={'pm25': 'hrrr_pm25'}, inplace=True)
        new_hrrr_dict_full = prep_new_hrrr_to_json(new_hrrr_df=hrrr_pa, hrrr_metadata=hrrr_metadata, original_hrrr_cols=original_hrrr_cols)
        with open(f'{results_folder_path}FIXED_{test_files[hr][0]}', 'w') as fout:
            json.dump(new_hrrr_dict_full, fout)





        # outputs and visualizations

        # fig, axs = plt.subplots(1,2, sharex=True, sharey=True)
        # axs[0].imshow(idw_pivoted, interpolation=None, origin='lower', extent=extent)

        # axs[1].scatter(california_pa.lon, california_pa.lat, s=5, c=california_pa.pm25)


        #%%


        # print(f'{dt.utcnow()} - override hrrr...')
        # new_hrrr, merged_df = override_hrrr_with_pa_average(hrrr_data, pa_nearest_gp_grouped)

        # print(f'{dt.utcnow()} - prepare new hrrr file...')
        # new_hrrr_dict_full = prep_new_hrrr_to_json(hrrr_data, new_hrrr, hrrr_metadata, original_hrrr_cols)

        # print(f'{dt.utcnow()} - saving new file...')
        # # save file
        # with open(f'{results_folder_path}FIXED_{test_files[hr][0]}', 'w') as fout:
        #     json.dump(new_hrrr_dict_full, fout)

        # print(f'{dt.utcnow()} - statistics...')
        # # statistics
        # visualize_stats(merged_df, hrrr_fn)

