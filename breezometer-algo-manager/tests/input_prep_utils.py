import pandas as pd
import numpy as np
import json
from osgeo import gdal


def load_stations_data(file_path):
    with open(file_path, 'r') as f:
        measurements = pd.DataFrame.from_records(json.load(f)['measurements'])
        measurements['datetime'] = pd.to_datetime(measurements['datetime'],
                                                  format='%Y-%m-%dT%H:%M:%S')
    if measurements is not None:
        # Remove non hourly polutants
        mask = ~measurements['pollutant_name'].str.contains('hr')
        measurements = measurements[mask]

        # Pivot measurements
        measurements = pd.pivot_table(measurements, values=['value'],
                                      index=['lat', 'lon', 'datetime'],
                                      columns=['pollutant_name'])
        measurements.columns = measurements.columns.droplevel(0)
        measurements.reset_index(drop=False, inplace=True)

        # Unite very close stations
        # NOTE: We assume that `measurements` containes data from one hour
        #       only, so there is no need for `normalize_timestamps`.
        #       The `unite_close_stations` averges any mid-hour data as well.
        measurements = unite_close_stations(measurements)

        if measurements.shape[0] == 0:
            measurements = None

    return measurements


def unite_close_stations(data):
    """Average data for stations closer than 0.00001 degrees"""
    data['station_id'] = (
        (data.lat * 10000).astype(int).astype(str).str.cat(
            (data.lon * 10000).astype(int).astype(str), sep='_'
        )
    )
    data_grouped = data.groupby('station_id').mean()
    return data_grouped.reset_index(drop=True, inplace=False)


def _get_cams_models(pollutants, cams_location, type):
    cams_data = None
    de_smoked = False
    hrrr_masked = False
    if cams_location:
        cams_raw = load_data_json(cams_location)
        if 'metadata' in cams_raw:
            de_smoked = cams_raw['metadata'].get('de_smoked', False)
            hrrr_masked = cams_raw['metadata'].get('hrrr_masked', False)

        cams_full = cams_raw['values']
        cams_data = list()
        for c in cams_full:
            if not c[type]:
                continue
            interesting_pollutant = dict()
            for p, v in c[type].items():
                if p not in pollutants or v is None:
                    continue
                interesting_pollutant[p] = v
            if interesting_pollutant:
                cams_data.append({
                    'lat': c['lat'],
                    'lon': c['lon'],
                })
                cams_data[-1].update(interesting_pollutant)
    return (de_smoked, hrrr_masked, cams_data) if cams_data else (None, None, None)


def load_cams_data(file_path):
    """Read CAMS JSON file into DataFrame"""
    cams_j = json.load(open(file_path))['values']
    cams_l = [{'lat': x['lat'], 'lon':x['lon'], **x['pollutants']} for x in cams_j]  # noqa: E501
    return pd.DataFrame(cams_l)


def load_traffic_data(file_path):
    traffic = pd.DataFrame.from_records(
        json.load(open(file_path))['traffic_pollution_grid'])
    # traffic.drop(['delta_lat', 'delta_lon'], axis=1, inplace=True)
    # pollutants = set(traffic.columns).difference({'lat', 'lon'})

    # if len(pollutants) == 0:
    #     traffic = None
    return traffic


def load_data_json(location):
    with open(location) as f:
        return json.load(f)


def get_hrrr_raw(file_path, pollutants=['pm25', 'pm10', 'co']):
    hrrr_raw = load_data_json(file_path)

    hrrr_full = hrrr_raw['values']

    hrrr_data = list()
    for c in hrrr_full:
        interesting_pollutants = dict()
        for p, v in c['pollutants'].items():
            if p not in pollutants or v is None:
                continue
            interesting_pollutants[p] = v
        if interesting_pollutants:
            hrrr_data.append({
                'lat': c['lat'],
                'lon': c['lon'],
            })
            hrrr_data[-1].update(interesting_pollutants)
    return hrrr_data


def get_hrrr_results(file_path):
    hrrr_base_data = get_hrrr_raw(file_path)
    if hrrr_base_data:
        full_hrrr = pd.DataFrame(hrrr_base_data, dtype=np.float)
        full_hrrr = full_hrrr.sort_values(['lat', 'lon'])
        full_hrrr = full_hrrr.reset_index(drop=True)
    return full_hrrr


def get_fmi_results(file_path):
    fmi = pd.read_parquet(file_path)
    fmi = fmi.sort_values(['lat', 'lon'])
    fmi = fmi.reset_index(drop=True)
    return fmi


def get_lu_weights(file_path, erode_niter=15):
    # Load tiff file
    ds = gdal.Open(file_path)
    nda = np.flipud(ds.ReadAsArray())

    # Extract the bbox of the data
    tiff_bbox = ds.GetGeoTransform()
    tiff_res = tiff_bbox[1]
    tiff_lon_min = tiff_bbox[0]
    tiff_lon_max = round(tiff_lon_min + tiff_res * (nda.shape[1] - 1), 1)
    tiff_lat_max = tiff_bbox[3]
    tiff_lat_min = round(tiff_lat_max - tiff_res * (nda.shape[0] - 1), 1)

    # Construct the LU grid
    weight_lat = np.linspace(tiff_lat_min, tiff_lat_max, num=nda.shape[0])
    weight_lon = np.linspace(tiff_lon_min, tiff_lon_max, num=nda.shape[1])
    xx, yy = np.meshgrid(weight_lon, weight_lat)

    # Convert to the DataFrame format expected by algoman
    weight = pd.DataFrame({
        'lat': yy.flatten(), 'lon': xx.flatten(),
        'weight': nda.flatten()
        }).sort_values(['lat', 'lon'])

    return weight


def get_data_by_bbox(data, west, south, east, north):
    measurements = None
    if data is not None:
        fm = data
        m_bbox = fm[(fm.lat >= south) & (fm.lon >= west) &
                    (fm.lat <= north) & (fm.lon <= east)]
        if len(m_bbox) > 0:
            measurements = m_bbox
            measurements.reset_index(drop=True, inplace=True)
            measurements.sort_values(['lat', 'lon'], inplace=True)
    return measurements
