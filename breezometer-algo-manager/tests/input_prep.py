"""
Creating inputs for algorithm tests, data goes through similar preprocess
as in the algorithm.
NOTICE: Fill in the required variable where the comment is: ### comment ###
"""

import pickle
import pandas as pd
import numpy as np
from input_prep_utils import (load_stations_data,
                              load_cams_data,
                              load_traffic_data,
                              get_hrrr_results,
                              get_fmi_results,
                              get_lu_weights,
                              get_data_by_bbox)
from geo_info import GeoInfo
import math
from pathlib import Path


#%% INIT geo info tile

### Enter Tile name ###
tile_name = 'Lyon'.lower().replace(' ', '_')
### Enter tile index ###
tile_index = (606, 820)

bbox = (-180.0, -90, 180, 90)
geo_info = GeoInfo(bbox, grid_distance_meters=500,
                   tile_size_lat=50,
                   tile_size_lon=50)

tile_to_do = geo_info
tile_to_do = geo_info.get_tile_by_tile_indexes(tile_index[0], tile_index[1])


#%% LOAD inputs. all inputs are taken from algo input besides the Land cover.
inputs_dir = Path('inputs/')

### Enter path for Stations data ###
stn_path = inputs_dir / 'measurements_2020-06-02T03-00-00.json'
stn = load_stations_data(stn_path)

### Enter path for CAMS global data ###
camsg_path = inputs_dir / '2020-06-02T03_00_00_global_cams.json'
camsg = load_cams_data(camsg_path)

### Enter path for CAMS regional data ###
camsr_path = inputs_dir / '2020-06-02T03_00_00_regional_cams.json'
camsr = load_cams_data(camsr_path)

### Enter path for Traffic data ###
traffic_path = inputs_dir / 'traffic_2020-06-02T03-00-00.json'
traffic = load_traffic_data(traffic_path)

### Enter path for Smoke Regional data ###
hrrr_path = inputs_dir / '2020-06-02T03_00_00_hrrr.json'
hrrr = get_hrrr_results(hrrr_path)

### Enter path for Smoke Global ###
fmi_path = inputs_dir / 'fmi_2020-06-02T03-00-00.parquet'
fmi = get_fmi_results(fmi_path)

### Enter path for Land cover data, make sure it mathces the tile ###
lc_path = inputs_dir / 'urban_raster-calc_v2_mask_EU.tif'
lc = get_lu_weights(lc_path.as_posix())


#%% CROP data to sizes required in the Algorithm

# degrees in km at the equator
distance_0_4 = 44.490537028437856
distance_0_1 = 11.122634257109464
distance_0_25 = 27.80658564277366
distance_0_027 = 3.0031112494195553
distance_0_01 = 1.1132

MEASUREMENTS_ADW_ED = 100

NUMBER_OF_CAMS_POINTS = 2
CAMS_GLOBAL_ED = math.ceil(distance_0_4 * NUMBER_OF_CAMS_POINTS)
CAMS_REGIONAL_ED = math.ceil(distance_0_1 * NUMBER_OF_CAMS_POINTS)

NUMBER_OF_HRRR_POINTS = 20
HRRR_ED = math.ceil(distance_0_027 * NUMBER_OF_HRRR_POINTS) + MEASUREMENTS_ADW_ED

NUMBER_OF_FMI_POINTS = 7
FMI_ED = math.ceil(distance_0_1 * NUMBER_OF_FMI_POINTS) + MEASUREMENTS_ADW_ED

NUMBER_OF_LC_POINTS = 2
LC_ED = math.ceil(distance_0_01 * NUMBER_OF_LC_POINTS)

# crop to bbox
inputs = {}
inputs['stations'] = get_data_by_bbox(
    stn, **tile_to_do.get_effective_bounding_box(MEASUREMENTS_ADW_ED))
inputs['cams_global'] = get_data_by_bbox(
    camsg, **tile_to_do.get_effective_bounding_box(CAMS_GLOBAL_ED))
inputs['cams_regional'] = get_data_by_bbox(
    camsr, **tile_to_do.get_effective_bounding_box(CAMS_REGIONAL_ED))
inputs['smoke_global'] = get_data_by_bbox(
    fmi, **tile_to_do.get_effective_bounding_box(FMI_ED))
inputs['smoke_regional'] = get_data_by_bbox(
    fmi, **tile_to_do.get_effective_bounding_box(HRRR_ED))
inputs['land_cover_weights'] = get_data_by_bbox(
    lc, **tile_to_do.get_effective_bounding_box(LC_ED))

# CREATE grid for tile
bbox = tile_to_do.get_tile_geo_bbox()
lats, lons = np.meshgrid(np.linspace(bbox[1], bbox[3], num=50),
                         np.linspace(bbox[0], bbox[2], num=50))
inputs['grid'] = pd.DataFrame({
    'lat': lats.flatten(),
    'lon': lons.flatten()
    })


#%% SAVE to Pickle
### Change path if needed ###
with open(f'inputs/input_data_{tile_name}.pkl', 'wb') as handle:
    pickle.dump(inputs, handle, protocol=pickle.HIGHEST_PROTOCOL)
