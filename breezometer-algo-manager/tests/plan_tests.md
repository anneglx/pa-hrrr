# Plan tests for the algoman library

Here are some ideas for how to create tests for this library.


## How to do this in the code:
- make presets of input combinations
- each test function should address a specific part of the code/logic
- each test function will use a relevant Manager() instance
- the test functions will be grouped together visually by context
- make sure to test all relevant input combinations in different areas, e.g. USA, EU, China...


## Notes
- There might be some ideas about how to test the edges of smoke regional - look in Shaked's research code / local dev drafts.
