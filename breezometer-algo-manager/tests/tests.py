import numpy as np
import pandas as pd
import pickle

from algoman.manager import Manager
from algoman.conversion import km2r
from unittest.mock import patch


# %%
def get_inputs(where, which_type='pickle'):
    if which_type == 'pickle':
        inputs = get_inputs_pickle(where)
        return inputs

    elif which_type == 'random':
        inputs = get_inputs_random(where)
        return inputs

    else:
        raise Exception(f'Inputs type "{which_type}" not recognized. '
                        f'"which_type" should be one of: "pickle", "random".')


def get_inputs_pickle(where):
    if where == 'usa':
        fname = 'input_data_phoenix.pkl'
    elif where == 'eu':
        fname = 'input_data_lyon.pkl'
    else:
        raise Exception(f'Inputs are not available at "{where}". '
                        f'"where" should be one of: "usa", "eu".')

    with open(f'tests/inputs/{fname}', 'rb') as f:
        inputs = pickle.load(f)

    return inputs


def get_inputs_random(where):
    """Make some dummy data. No guarantee it'll work."""
    if where == 'usa':
        lat_range = [33.39703, 33.62179]
        lon_range = [-112.34516, -112.1204]
    elif where == 'eu':
        lat_range = [46.20874, 46.43351]
        lon_range = [4.30886, 4.53362]
    elif where == 'isr':
        lat_range = [32.6, 32.7]
        lon_range = [34.9, 35.1]
    else:
        raise Exception(f'Inputs are not available at "{where}". '
                        f'"where" should be one of: "usa", "eu", "isr".')

    inputs = {}

    lats, lons = np.meshgrid(np.linspace(lat_range[0], lat_range[1], num=50),
                             np.linspace(lon_range[0], lon_range[1], num=50))
    inputs['grid'] = pd.DataFrame({
        'lat': lats.flatten(),
        'lon': lons.flatten()
        })

    lats, lons = np.meshgrid(np.random.uniform(low=lat_range[0], high=lat_range[1], size=5),
                             np.random.uniform(low=lon_range[0], high=lon_range[1], size=5))
    inputs['stations'] = pd.DataFrame({
        'lat': lats.flatten(),
        'lon': lons.flatten(),
        'pm25': np.random.uniform(low=0, high=100, size=5 ** 2)
        })

    lats, lons = np.meshgrid(np.linspace(lat_range[0], lat_range[1], num=7),
                             np.linspace(lon_range[0], lon_range[1], num=7))
    inputs['cams_global'] = pd.DataFrame({
        'lat': lats.flatten(),
        'lon': lons.flatten(),
        'pm25': np.random.uniform(low=0, high=100, size=7 ** 2)
        })

    lats, lons = np.meshgrid(np.linspace(lat_range[0], lat_range[1], num=20),
                             np.linspace(lon_range[0], lon_range[1], num=20))
    inputs['land_cover_weights'] = pd.DataFrame({
        'lat': lats.flatten(),
        'lon': lons.flatten(),
        'weight': np.random.uniform(low=1, high=15, size=20 ** 2)
        })

    lats, lons = np.meshgrid(np.linspace(lat_range[0], lat_range[1], num=15),
                             np.linspace(lon_range[0], lon_range[1], num=15))
    inputs['smoke_regional'] = pd.DataFrame({
        'lat': lats.flatten(),
        'lon': lons.flatten(),
        'pm25': np.random.uniform(low=0, high=100, size=15 ** 2)
        })

    inputs['smoke_global'] = None

    lats, lons = np.meshgrid(np.linspace(lat_range[0], lat_range[1], num=7),
                             np.linspace(lon_range[0], lon_range[1], num=7))
    inputs['smoke_global'] = pd.DataFrame({
        'lat': lats.flatten(),
        'lon': lons.flatten(),
        'pm25': np.random.uniform(low=0, high=100, size=7 ** 2)
        })

    return inputs


# %% Smoke - Example

# def test_smoke_models_coverage():
#     """
#     TODO: docstring
#     this is only an example, will always break
#     """
#     # test in usa...
#     inputs = get_inputs(where='usa')  # , smoke_r_low=True, smoke_g_high=True)
#     inputs.pop('land_cover_weights')

#     manager = Manager(**inputs)
#     manager.run_algos()
#     res_smoke_usa = manager.results['smoke']['pm25'].max()
#     expect_smoke_usa = inputs['smoke_global']['pm25'].max()

#     assert np.isclose(res_smoke_usa, expect_smoke_usa, rtol=0.01)  # 1% of expected

#     # test in eu...
#     inputs = get_inputs(where='eu')  # , smoke_r_low=True, smoke_g_high=True)
#     inputs.pop('land_cover_weights')

#     manager = Manager(**inputs)
#     manager.run_algos()
#     res_smoke_eu = manager.results['smoke']['pm25'].max()
#     expect_smoke_eu = inputs['smoke_regional']['pm25'].max()

#     assert np.isclose(res_smoke_eu, expect_smoke_eu, rtol=0.01)  # 1% of expected


# %% Land Cover

def test_stn_distances_defaults():
    """
    Check that `manager.max_station_distance` and `manager.min_station_distance`
    get their default values when the input does not include smoke models
    or land cover weights.

    Since there is a special handling for these distances in Europe, we run
    this test for both USA and Europe.
    """
    # USA - max distance is expected to be 100km
    inputs = get_inputs(where='usa')
    inputs.pop('smoke_regional')
    inputs.pop('smoke_global')
    inputs.pop('land_cover_weights')

    manager = Manager(**inputs)
    manager.run_algos()
    res_maxd_usa = np.round(manager.max_station_distance, 5)
    res_mind_usa = np.round(manager.min_station_distance, 5)
    expect_maxd_usa = round(km2r(100), 5)
    expect_mind_usa = round(km2r(30), 5)

    assert np.isscalar(res_maxd_usa)
    assert np.isscalar(res_mind_usa)
    assert np.isclose(res_maxd_usa, expect_maxd_usa, rtol=0.01)  # 1km difference
    assert np.isclose(res_mind_usa, expect_mind_usa, rtol=0.01)  # 1km difference

    # Europe - max distance is expected to be 100km
    inputs = get_inputs(where='eu')
    inputs.pop('smoke_regional')
    inputs.pop('smoke_global')
    inputs.pop('land_cover_weights')

    manager = Manager(**inputs)
    manager.run_algos()
    res_maxd_eu = np.round(manager.max_station_distance, 5)
    res_mind_eu = np.round(manager.min_station_distance, 5)
    expect_maxd_eu = round(km2r(100), 5)
    expect_mind_eu = round(km2r(30), 5)

    assert np.isscalar(res_maxd_eu)
    assert np.isscalar(res_mind_eu)
    assert np.isclose(res_maxd_eu, expect_maxd_eu, rtol=0.01)  # 1km difference
    assert np.isclose(res_mind_eu, expect_mind_eu, rtol=0.01)  # 1km difference


def test_stn_distances_with_lc():
    """
    Check that `manager.max_station_distance` and `manager.min_station_distance`
    get their expected values when the input includes land cover weights but
    not smoke models.

    Since there is a special handling for these distances in Europe, we run
    this test for both USA and Europe.
    """
    # USA - max distance is expected to be 200km
    # min distance is expected to be 10km
    inputs = get_inputs(where='usa')
    inputs.pop('smoke_regional')
    inputs.pop('smoke_global')

    manager = Manager(**inputs)
    manager.run_algos()
    res_maxd_usa = np.round(manager.max_station_distance, 5)
    res_mind_usa = np.round(manager.min_station_distance, 5)
    expect_maxd_usa = round(km2r(200), 5)
    expect_mind_usa = round(km2r(10), 5)

    assert type(res_maxd_usa) == np.ndarray
    assert type(res_mind_usa) == np.ndarray
    assert np.isclose(res_maxd_usa, expect_maxd_usa, rtol=0.01).all()  # 1km difference
    assert np.isclose(res_mind_usa, expect_mind_usa, rtol=0.01).all()  # 1km difference

    # Europe - max distance is expected to be 100km
    # min distance is expected to be 10km
    inputs = get_inputs(where='eu')
    inputs.pop('smoke_regional')
    inputs.pop('smoke_global')

    manager = Manager(**inputs)
    manager.run_algos()
    res_maxd_eu = np.round(manager.max_station_distance, 5)
    res_mind_eu = np.round(manager.min_station_distance, 5)
    expect_maxd_eu = round(km2r(100), 5)
    expect_mind_eu = round(km2r(10), 5)

    assert type(res_maxd_eu) == np.ndarray
    assert type(res_mind_eu) == np.ndarray
    assert np.isclose(res_maxd_eu, expect_maxd_eu, rtol=0.01).all()  # 1km difference
    assert np.isclose(res_mind_eu, expect_mind_eu, rtol=0.01).all()  # 1km difference


def test_stn_distances_with_smoke_and_lc():
    """
    Check that `manager.max_station_distance` and `manager.min_station_distance`
    get their expected values when the input includes smoke models and land
    cover weights.

    Since there is a special handling for these distances in Europe, we run
    this test for both USA and Europe.
    """
    # USA
    # max distance is expected to be in the range 200-30km
    # min distance is expected to be 10km
    inputs = get_inputs(where='usa')

    manager = Manager(**inputs)
    manager.run_algos()
    res_maxd_usa = np.round(manager.max_station_distance, 5)
    res_mind_usa = np.round(manager.min_station_distance, 5)
    expect_maxd_usa = [round(km2r(30), 5), round(km2r(200), 5)]
    expect_mind_usa = round(km2r(10), 5)

    assert type(res_maxd_usa) == np.ndarray
    assert type(res_mind_usa) == np.ndarray
    # all maxd are within [30, 200]
    assert np.bitwise_and(
        res_maxd_usa >= expect_maxd_usa[0],
        res_maxd_usa <= expect_maxd_usa[1],
        ).all()
    # at least one maxd is within [100, 200]
    # this test is specific for the hour we chose, it might not pass in case
    # of high smoke levels
    expect_maxd_midrange_usa = [round(km2r(100), 5), round(km2r(200), 5)]
    assert np.bitwise_and(
        res_maxd_usa >= expect_maxd_midrange_usa[0],
        res_maxd_usa <= expect_maxd_midrange_usa[1],
        ).any()
    # all mind == 10
    assert np.isclose(res_mind_usa, expect_mind_usa, rtol=0.01).all()  # 1km difference

    # Europe
    # max distance is expected to be in the range 100-30km
    # min distance is expected to be 10km
    inputs = get_inputs(where='eu')

    manager = Manager(**inputs)
    manager.run_algos()
    res_maxd_eu = np.round(manager.max_station_distance, 5)
    res_mind_eu = np.round(manager.min_station_distance, 5)
    expect_maxd_eu = [round(km2r(30), 5), round(km2r(100), 5)]
    expect_mind_eu = round(km2r(10), 5)

    assert type(res_maxd_eu) == np.ndarray
    assert type(res_mind_eu) == np.ndarray
    # all maxd are within [30, 100]
    assert np.bitwise_and(
        res_maxd_eu >= expect_maxd_eu[0],
        res_maxd_eu <= expect_maxd_eu[1],
        ).all()
    # all mind == 10
    assert np.isclose(res_mind_eu, expect_mind_eu, rtol=0.01).all()  # 1km difference


def test_stn_distances_with_lc_and_high_conc():
    """
    Check that `manager.max_station_distance` gets the expected value when
    the input:
        - includes land cover weights
        - includes a station with high concentration
        - does not necessarily include smoke models

    In these situations the maxd should get a custom value (e.g. 100km instead
    of 200km).
    This behaviour should be most visible in locations outside Europe, since
    inside Europe the maxd is already custom.
    """
    inputs = get_inputs(where='usa')
    inputs.pop('smoke_regional')
    inputs.pop('smoke_global')

    # Modify some values to be over the thresholds
    idx_to_modify = [0, 1]
    modified_idx = np.zeros(len(inputs['stations']))
    modified_idx[idx_to_modify] = 1
    modified_idx = modified_idx.astype(bool)

    inputs['stations']['pm25'].iloc[modified_idx] = 700
    inputs['stations']['o3'].iloc[modified_idx] = 400

    manager = Manager(**inputs)
    manager.run_algos()

    res_maxd = np.round(manager.max_station_distance, 5)
    assert type(res_maxd) == np.ndarray

    res_maxd_orig = res_maxd[~modified_idx]
    res_maxd_mod = res_maxd[modified_idx]
    expect_maxd_orig = round(km2r(200), 5)
    expect_maxd_mod = round(km2r(100), 5)
    assert np.isclose(res_maxd_orig, expect_maxd_orig, rtol=0.01).all()  # 1km difference
    assert np.isclose(res_maxd_mod, expect_maxd_mod, rtol=0.01).all()  # 1km difference


# stations layer weights - with LC and w/o
def test_lc_was_used():
    """
    When the input `land_cover_weights` is included, we expect it to be
    incorporated into the weights that combine stations and CAMS layers.
    Here we verify that the relevant function `stations_weighter` is called
    only when the input contains the land cover weights.
    """
    # With land conver weights
    with patch('algoman.manager.stations_weighter') as mock_stations_weighter:
        inputs = get_inputs(where='usa')
        manager = Manager(**inputs)
        manager.run_algos()
        manager.run_layer_weighters()
        mock_stations_weighter.assert_called()

    # Without land conver weights
    with patch('algoman.manager.stations_weighter') as mock_stations_weighter:
        inputs = get_inputs(where='usa')
        inputs.pop('land_cover_weights')
        manager = Manager(**inputs)
        manager.run_algos()
        manager.run_layer_weighters()
        mock_stations_weighter.assert_not_called()
