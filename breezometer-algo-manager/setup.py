from setuptools import setup, find_packages


setup(
    name='algoman',
    version='4.2.1',
    description='Algorithm Manager',

    url='https://bitbucket.org/breezometer/algo-manager/',

    author='BreezoMeter',
    author_email='developers@breezometer.com',

    packages=find_packages(),

    install_requires=[
        'numpy>=1.17.2',
        'pandas>=0.25.1',
        'scipy>=1.3.1'
    ]
)
