# LEO - Lizard Eye Occlusion

The weight of station $i$ at location $x$ is

$$\begin{equation}
    W_{s_i}(x) =
        {{IDW}_{s_i}(x)
            \cdot
        \prod_{j \neq i} {O_{s_i}(x,s_j)}}
\end{equation}$$

where $IDW_{s_i}(x)$ is the squared inverse distance to station $i$ at location $x$; and $O_{s_i}(x,s_j)$ is the occlusion factor of station $i$ by station $j$ at location $x$.

---

The squared inverse distance between point $x$ and station $i$ is

$$\begin{equation}
    {IDW}_{s_i}(x) = {{1} \over {d^2(x,s_i)}}
\end{equation}$$

where $d(x,s_i)$ is the distance between point $x$ and station $i$.

---

The occlusion factor of station $i$ by station $j$ at location $x$ is

$$\begin{equation}
    {O_{s_i}(x,s_j)} =
        \begin{cases}
            1, & d(x,s_i) < d(x,s_j) \\
            1, & \theta > {\pi \over 2} \\
            {\theta \over {\pi / 2}} ^
                    {1 - {d(x,s_j) \over d(x,s_i)}}, & \text{otherwise}
        \end{cases}
\end{equation}$$

where $d(x,s_i)$, $d(x,s_j)$ are the distances between point $x$ and stations $i$, $j$; and $\theta$ is the angle between stations $i$ and $j$ through point $x$.  Note that the occlusion factor lies within the closed interval $[0,1]$.

---

Station weights are normalized such that

$$\begin{equation}
    \hat{W}_{s_i}(x) =
        {{W_{s_i}(x)}
            \over
        {\sum_{i=1..n}{W_{s_i}(x)}}}
\end{equation}$$

---

The final concentration at point $x$ is

$$\begin{equation}
    C(x) = \sum_{i=1..n}{\hat{W}_{s_i}(x) \cdot C_{s_i}}
\end{equation}$$

where $C_{s_i}$ is the concentration measured at station $i$.
