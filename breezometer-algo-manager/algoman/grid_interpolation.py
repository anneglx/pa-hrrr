import numpy as np
import pandas as pd

from scipy.interpolate import pchip, interp2d
from .interpolation import SplineInterpolator


def interpolate_grid2grid(grid, data, method, pwise_extrapolate=None):
    """
    Generate interpolated grids for external data (e.g. the CAMS model).

    This function was intended for data which is originally on regular grids.
    Some of the methods provided here might also work for data on non-regular
    grids (i.e. with variyng gaps) but this behaviour is un-tested.

    Parameters
    ----------
    data : dataframe
        The external data. Must contain `lat` and `lon` columns.
    grid : dataframe
        Grid for interpolation. Must contain `lat` and `lon` columns.
    method : {'pwise', 'rsbs', 'linear'}
        Interpolation method. Piecewise, RectSphereBivariateSpline, or Linear.
        Defaults to 'pwise'.
    pwise_extrapolate : {bool, 'periodic', None}, optional
        Determines whether the `pchip` function performs extrapolation and how.
        From the documentation of scipy.interpolate.PchipInterpolator.__call__:
        If bool, determines whether to extrapolate to out-of-bounds points
        based on first and last intervals, or to return NaNs. If ‘periodic’,
        periodic extrapolation is used. If None (default), use
        self.extrapolate.

    Returns
    -------
    results: dataframe
        The data interpolated over grid.
    """
    # Initialize interpolator
    pollutants = set(data.columns) - {'lat', 'lon'}

    if method == 'rsbs':
        # RectSphereBivariateSpline
        intrp = SplineInterpolator(data, pollutants)

        # Interpolate pollutants over grid (and clip negative values to zero)
        results = {pol: intrp(grid, pol).clip(0) for pol in pollutants}

        # Format results as DataFrame and append coordinates
        results = pd.concat((grid[['lat', 'lon']], pd.DataFrame(results)), axis=1)  # noqa: E501

    elif method == 'pwise':
        # Piecewise interpolation
        l = [pwise_pol(grid, data, pol, pwise_extrapolate) for pol in pollutants]  # noqa: E501
        results = pd.concat([grid, pd.concat(l, axis=1)[list(pollutants)]], axis=1)  # noqa: E501

    elif method == 'linear':
        # Linear interpolation
        # TODO: Unoptimized for regular grid. (1) Add boolean `regular_grid`
        # parameter to this function. (2) Add mechanism to infer whether grid
        # is regular, to be run on Manager init.
        data_lons = data.lon.unique()
        data_lats = data.lat.unique()
        data_values = data.pivot(index='lat', columns='lon')

        results = {'lat': grid.lat.values, 'lon': grid.lon.values}
        for pol in pollutants:
            # Create interpolator
            interpolator = interp2d(data_lons, data_lats, data_values[pol],
                                    kind='linear', bounds_error=True)

            # Interpolate to grid (in a loop, unoptimized for regular grid)
            pol_results = []
            for gridpoint in grid.itertuples():
                pol_results.append(interpolator(gridpoint.lon, gridpoint.lat)[0])  # noqa: E501
            results.update({pol: pol_results})
        results = pd.DataFrame(results)

    else:
        raise Exception("Legal interpolation methods: 'pwise', 'rsbs', or 'linear'")  # noqa: E501

    return results


def pwise_pol(grid, data, pol, extrapolate):
    """
    Interpolate data to grid using per-axis-piecewise bicubic splines

    Piecewise-interpolation is a 1D concept. There's no way to apply it on a
    rectagular cell in a 2d plane, since a cell has 4 corners, but a surface
    is defined by 3 points. To overcome this, we perform piecewise
    interpolation on one axis, and then piecwise-interpolate the results on the
    other axis.

    Note:
        `extrapolate` is sent into `pchip.__call__`, not into `pchip` itself.
    """
    # Coordinates for interpolation loops
    grid_lons = np.unique(grid.lon)
    grid_lats = np.unique(grid.lat)
    data_lons = np.unique(data.lon)

    lats, lons, pols = _np_pwise_pol(
        grid_lons, grid_lats, data_lons,
        data.loc[:, 'lat'].values, data.loc[:, 'lon'].values,
        data.loc[:, pol].values,
        extrapolate
    )
    return pd.DataFrame({'lat': lats, 'lon': lons, pol: pols}).sort_values(
        ['lat', 'lon']
    ).reset_index(drop=True)


def _np_pwise_pol(
        grid_unique_lons, grid_unique_lats, data_unique_lons,
        data_mesh_lats, data_mesh_lons, data_mesh_pol, extrapolate
):
    """
    Interpolate data to grid using per-axis-piecewise bicubic splines

    (see pwise_pol for complete explanation)

    This is a pure numpy/scipy interpolation in order to save some time
    (especially on small interpolations).


    Note:
        `extrapolate` is sent into `pchip.__call__`, not into `pchip` itself.

    Parameters
    ----------
    grid_unique_lons : np.ndarray[float]
        The grid longitudes (unique), so only one occurrence of each grid
        longitude
        shape: (num_of_unique_grid_longitudes, ) in production (50, )
    grid_unique_lats : np.ndarray[float]
        The grid latitude (unique), so only one occurrence of each grid
        longitude
        shape: (num_of_unique_grid_latitudes, ) in production (50, )
    data_unique_lons : np.ndarray[float]
        The data longitudes (unique), so only one occurrence of each data
        longitude
        shape: (num_of_unique_data_lons, ) in production depends
                on model, on global cams for example (5, )
    data_mesh_lats : np.ndarray[float]
        The data latitude flatten mesh grid,
        data_mesh_lat, data_mesh_lons, and data_mesh_pol must match as a
        single mesh grid that if reshaped in the same way should always match
        shape: (num_of_unique_data_lons * num_of_unique_data_lats, )
    data_mesh_lons : np.ndarray[float]
        The data longitude flatten mesh grid
        data_mesh_lat, data_mesh_lons, and data_mesh_pol must match as a
        single mesh grid that if reshaped in the same way should always match
        shape: (num_of_unique_data_lons * num_of_unique_data_lats, )
    data_mesh_pol : np.ndarray[float]
        The data pollutant levels flatten mesh grid
        data_mesh_lat, data_mesh_lons, and data_mesh_pol must match as a
        single mesh grid that if reshaped in the same way should always match
        shape: (num_of_unique_data_lons * num_of_unique_data_lats, )
    extrapolate : {bool, 'periodic', None}
        Determines whether the `pchip` function performs extrapolation and how.
        From the documentation of scipy.interpolate.PchipInterpolator.__call__:
        If bool, determines whether to extrapolate to out-of-bounds points
        based on first and last intervals, or to return NaNs. If ‘periodic’,
        periodic extrapolation is used. If None (default), use
        self.extrapolate.

    Returns
    -------
    lats: np.ndarray[float]
        latitude section of grid as a mesh grid part, every lat will repeat
        unique longitude number of time, and each time would be paired with
        a unique longitude in the lons return value
        shape: grid_size = num_uniq_grid_lats * num_unique_grid_lons
               (grid_siae, ) in production (2500, )
    lons: np.ndarray[float]
        longitude section of grid as a mesh grid part, every lon will repeat
        unique latitude number of time, and each time would be paired with
        a unique latitude in the lats return value
        shape: grid_size = num_uniq_grid_lats * num_unique_grid_lons
               (grid_siae, ) in production (2500, )
    pol: np.ndarray[float]
        The pwise interpolated values of the pollutant as a flattened grid
        matching the lat, lon in the lats lons return value
        shape: grid_size = num_uniq_grid_lats * num_unique_grid_lons
               (grid_siae, ) in production (2500, )
    """
    lats = []
    lons = []
    pol = []
    # Interpolate to (grid_lats, data_lons)
    for lon in data_unique_lons:
        row_predicate = data_mesh_lons == lon
        row_lat = data_mesh_lats[row_predicate]
        row_pol = data_mesh_pol[row_predicate]
        lats.append(grid_unique_lats)
        lons.append(np.full_like(grid_unique_lats, lon, dtype=grid_unique_lats.dtype))
        intrp = pchip(row_lat, row_pol)
        pol.append(intrp(grid_unique_lats, extrapolate=extrapolate))

    inter_lats = np.concatenate(lats)
    inter_lons = np.concatenate(lons)
    inter_pol = np.concatenate(pol)

    lats = []
    lons = []
    pol = []
    # Interpolate to (grid_lats, grid_lons)
    for lat in grid_unique_lats:
        row_predicate = inter_lats == lat
        row_lon = inter_lons[row_predicate]
        row_pol = inter_pol[row_predicate]
        lats.append(np.full_like(grid_unique_lons, lat, dtype=grid_unique_lons.dtype))
        lons.append(grid_unique_lons)
        intrp = pchip(row_lon, row_pol)
        pol.append(intrp(grid_unique_lons, extrapolate=extrapolate))

    final_lats = np.concatenate(lats)
    final_lons = np.concatenate(lons)
    final_pol = np.concatenate(pol)

    return final_lats, final_lons, final_pol
