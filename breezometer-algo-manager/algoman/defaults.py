from .conversion import km2r


# Stations ####################
MIN_STATION_DISTANCE = km2r(10)
MAX_STATION_DISTANCE = km2r(200)
SPATIAL_ALGO = 'leo'
# Min/max distance for cases without Land-cover
NO_LC_MIN_STATION_DISTANCE = km2r(30)
NO_LC_MAX_STATION_DISTANCE = km2r(100)
# Max distance for stations inside CAMS regional's bbox
EU_MAX_STATION_DISTANCE = km2r(100)
# Max distance for stations with very high concentrations
HIGH_CONC_MAX_STATION_DISTANCE = km2r(100)
HIGH_CONC_THRESHOLDS = {
    'pm25': 500,
    'pm10': 600,
    'o3':   306,
    'no2':  532,
    'no':   532,  # Same as no2
    'nox':  532,  # Same as no2
    'so2':  573,
    'co':   49785
    }  # Missing (not part of BAQI): c6h6, nmhc, trs

# CAMS ####################
CAMS_INTRP_METHOD = 'pwise'

# Smoke ####################
SMOKE_INTRP_METHOD = 'pwise'
SMOKE_PM10_FACTOR = 1.5
SMOKE_CALIBRATION_PARAMS = {
        'regional': {'clip_max': 2000.0, 'scale_factor': None},
        'global':   {'clip_max': 2000.0, 'scale_factor': 0.5}
        }
# Smallest Min/Max distance affected by smoke
SMALLEST_MAXD_SMOKE = km2r(30)
SMALLEST_MIND_SMOKE = km2r(10)
# Break points
SMOKE_CONC_BP = (0, 5, 20, 50, 100, 500, 1000)
SMOKE_LEVELS_BP = (0, 1, 2, 3, 4, 5, 6)
SMOKE_KERNEL_SIZES = {
        'regional': [36, 18, 72],
        'global': [10, 6, 20]
        }
