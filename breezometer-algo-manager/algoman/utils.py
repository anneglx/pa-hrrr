import numpy as np
import pandas as pd

from .constants import REGIONAL_CAMS_BOUNDS, REGIONAL_SMOKE_BOUNDS


# ALGORITHMS
def get_pollutants(stations):
    """Return set of pollutants from DataFrame"""
    # NOTE: Assumes only columns are lat, lon and pollutant columns
    pollutants = set(stations.columns).difference(('lat', 'lon'))
    return pollutants


def filter_stations_for_pollutant(stations, pollutant):
    """Filter out stations with NaNs for given pollutant"""
    idx = np.flatnonzero(~np.isnan(stations[pollutant]))
    return idx


def subset_weights(weights, mask):
    """
    Subset N-dimensional weights array with given 1D mask array

    Note that the _first_ dimension is not subset, as that's expected to be the
    grid dimension.
    """
    if weights.ndim == 2:
        return weights[:, mask]
    elif weights.ndim == 3:
        return weights[:, [[x] for x in mask], mask]
    else:
        raise NotImplementedError('Can only subset 2- or 3-dimensional arrays')


def format_results(grid, results):
    """Format results dict into DataFrame and append to grid (lat,lon)"""
    # Format results as DataFrame and append coordinates
    df = pd.concat((grid[['lat', 'lon']], pd.DataFrame(results)), axis=1)

    return df


# SMOKE
def get_bbox(data):
    return [data.lat.min(), data.lat.max(), data.lon.min(), data.lon.max()]


def coords_inside_regional_domain(coords, model_name):
    """
    Return whether ``coords`` is completely inside a regional model's bounds.

    Parameters
    ----------
    coords : DataFrame
        Locations we want to know if they intersect with a model. Columns
        should contain lat, lon.
    model_name : string
        The type of the model we compare against, to get the correct bounds.

    Returns
    -------
    Boolian
    """
    if model_name == 'cams':
        domain_bounds = REGIONAL_CAMS_BOUNDS
    elif model_name == 'smoke':
        domain_bounds = REGIONAL_SMOKE_BOUNDS
    else:
        return False

    coords_lon_min = coords.lon.min()
    coords_lat_min = coords.lat.min()
    coords_lon_max = coords.lon.max()
    coords_lat_max = coords.lat.max()

    reg_lon_min = domain_bounds[0]
    reg_lat_min = domain_bounds[1]
    reg_lon_max = domain_bounds[2]
    reg_lat_max = domain_bounds[3]

    inside = ((coords_lon_min >= reg_lon_min) and
              (coords_lon_max <= reg_lon_max) and
              (coords_lat_min >= reg_lat_min) and
              (coords_lat_max <= reg_lat_max))

    return inside


def pivot_data(data, index_='lat', columns_='lon', values_='pm25', fill_=0.0):
    """
    Pivot a DataFrame and return its (values, index, columns) separately.

    The default behaviour is:

    * index is latitude
    * columns are longitude
    * values are pm25
    """
    data_pvt = data.pivot_table(
            index=index_, columns=columns_, values=values_
            )
    data_pvt.fillna(fill_, inplace=True)
    return data_pvt.values, data_pvt.index.values, data_pvt.columns.values


# Manager, Smoke
def coords_outside_regional_domain(coords, model_name):
    """
    Return whether ``coords`` is entirely outside a regional model's bounds.

    Parameters
    ----------
    coords : DataFrame
        Locations we want to know if they intersect with a model. Columns
        should contain lat, lon.
    model_name : string
        The type of the model we compare against, to get the correct bounds.
    model_data : DataFrame, optional
        In case ``coords`` is not entirely outside the model's bounds, and we
        want to check whether it intersects with the model's data as well.
        This is useful in cases where the model's domain is not rectangular,
        for example with smoke data.
        Columns should contain lat, lon.

    Returns
    -------
    Boolian
    """
    if model_name == 'cams':
        domain_bounds = REGIONAL_CAMS_BOUNDS
    elif model_name == 'smoke':
        domain_bounds = REGIONAL_SMOKE_BOUNDS
    else:
        return False

    coords_lon_min = coords.lon.min()
    coords_lat_min = coords.lat.min()
    coords_lon_max = coords.lon.max()
    coords_lat_max = coords.lat.max()

    reg_lon_min = domain_bounds[0]
    reg_lat_min = domain_bounds[1]
    reg_lon_max = domain_bounds[2]
    reg_lat_max = domain_bounds[3]

    outside = ((coords_lon_max <= reg_lon_min) or
               (coords_lat_max <= reg_lat_min) or
               (coords_lon_min >= reg_lon_max) or
               (coords_lat_min >= reg_lat_max))

    return outside


# LAYER WEIGHTERS
def cos_weight(distance, min_d, max_d):
    """Calculate weights as cosine of distance matrix within provided range"""
    # NOTE: distance, min_d, max_d must be in the same units (kms, rads, etc.)
    # TODO: Handle NaNs in distance

    # Normalize distances within bounds from [min_d, max_d] to [0,pi]
    normalized_distance = distance - min_d
    normalized_distance = normalized_distance * (np.pi / (max_d - min_d))

    # Cosine weigh and handle values outside bounds
    weight = (np.cos(normalized_distance) + 1) / 2
    weight[distance <= min_d] = 1
    weight[distance >= max_d] = 0

    return weight


def lin_weight(distance, min_d, max_d):
    """Calculate weights as linearily decreasing within provided range"""
    # Normalize distances within bounds from [min_d, max_d] to [0, 1]
    weight = (distance - min_d) / (max_d - min_d)
    weight = 1 - weight.clip(0, 1)

    return weight


def superellipse(x, y, x0=0, y0=0, a=1, b=1, n=4):
    """Compute Superellipse (defaults to squircle)"""
    return np.abs((x - x0) / a) ** n + np.abs((y - y0) / b) ** n
