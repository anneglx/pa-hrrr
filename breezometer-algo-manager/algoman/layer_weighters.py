import numpy as np
import pandas as pd

from .utils import superellipse, lin_weight, get_pollutants
from .spatial import cartesian_distance
from .constants import (REGIONAL_CAMS_BUFFER, REGIONAL_CAMS_CENTER,
                        REGIONAL_CAMS_RADIUS)


def regional_cams_weighter(grid, buffer_=REGIONAL_CAMS_BUFFER,
                           center=REGIONAL_CAMS_CENTER,
                           radius=REGIONAL_CAMS_RADIUS):
    """Weigh regional CAMS as function of squircle-distance from boundaries"""
    # Note that buffer_ is in degrees
    external = superellipse(x=grid['lon'].values, y=grid['lat'].values,
                            x0=center[1], y0=center[0],
                            a=radius[1], b=radius[0])

    internal = superellipse(x=grid['lon'].values, y=grid['lat'].values,
                            x0=center[1], y0=center[0],
                            a=radius[1] - buffer_, b=radius[0] - buffer_)

    # Weight as linearly decreasing from boundary to buffer_ degrees inwards
    weight = 2 - internal - external

    # Limit values to range [0, 1]
    weight = weight.clip(0, 1)

    # Format results as DataFrame and append coordinates
    results = pd.concat((grid[['lat', 'lon']],
                         pd.DataFrame({'weight': weight})), axis=1)
    return results


def adjust_station_max_distance_in_EU(mind, maxd, maxd_eu, stations):
    """
    Use a designated value of stations-max-distance for stations inside
    the CAMS regional’s bbox (e.g. Europe).
    In the border area between CAMS regional to global, the max distance
    will change according to the gradient of the weights between the layers.

    Parameters
    ----------
    mind : float/vector
        Minimum station distance.
    maxd : float/vector
        Maximum station distance.
    maxd_eu : float
        The maximum station distance in CAMS regional bbox.
    stations : DataFrame
        Stations in tile.

    Returns
    -------
    mind_vect : np.array(n, )
        Vector of the minimum distance for each station.
    maxd_vect : np.array(n, )
        Vector of the minimum distance for each station.

    """
    stn_dist = stations[['lat', 'lon']].copy()
    stn_dist['maxd'] = maxd
    stn_dist['mind'] = mind
    stn_dist['diff'] = abs(maxd - maxd_eu)

    # find the weights as a function of distance from layer boundries
    boundry_weights = regional_cams_weighter(stn_dist)
    # find relevant stations
    stn_with_weights = stn_dist.merge(boundry_weights, on=['lat', 'lon'],
                                      how='left')
    # calculate the relative max distance
    stn_with_weights['final_maxd'] = (
        stn_with_weights['maxd'] -
        (stn_dist['diff'] * stn_with_weights['weight'])
        )

    mind_vect = stn_with_weights['mind'].values
    maxd_vect = stn_with_weights['final_maxd'].values

    return mind_vect, maxd_vect


def adjust_max_distance_in_stations_w_high_conc(maxd, maxd_high_conc, stations,
                                                conc_thresholds):
    """
    This is done to prevent very large (200km) circles with low BAQI,
    in which land-cover is not visible.

    Parameters
    ----------
    maxd : np.ndarray
        Maximum station distances, as a vector.
    maxd_high_conc : int/float
        The new max distance for stations with high values.
    stations : pd.DataFrame
        Stations data, should have the columns lat, lon.
    conc_thresholds : dictionary
        Defines the concentration tresholds over which we adjust the max
        distances. Keys are pollutant names, values are concentration thresholds.

    Returns
    -------
    maxd_adjusted : np.ndarray
        Adjusted maximum station distacnes. Same shape as maxd..

    """
    pollutants = get_pollutants(stations)

    to_adjust = np.full_like(maxd, False, dtype=bool)  # Initialize with False
    for pol in pollutants:
        if pol in conc_thresholds.keys():
            # If any pol crosses the threshold - change the station to True
            to_adjust_pol = stations[pol].values > conc_thresholds[pol]
            to_adjust = np.bitwise_or(to_adjust, to_adjust_pol)

    # Update with the new maxd value
    maxd_adjusted = maxd.copy()
    maxd_adjusted[to_adjust] = maxd_high_conc

    return maxd_adjusted


def stations_weighter(grid, stations, min_d, max_d, weight_lc):
    """Wrapper for calculating weights by distance from stations and combine
       it with the land cover weights"""
    weight_dist = station_distance_weighter(grid, stations, min_d, max_d)
    weight_combined = combine_station_weights(weight_dist, weight_lc)
    return weight_combined


def station_distance_weighter(grid, stations, min_d, max_d):
    """Weigh grid as a function of distance from closest station in range"""
    # Grid-station distances
    distances = cartesian_distance(grid, stations)
    # Linearily decreasing weights
    # (weights for each grid point - per station)
    weights = lin_weight(distances, min_d, max_d)
    # Get the max weight
    # (weights for each grid point - per pollutant)
    max_weights = max_station_weight(weights, stations)
    results = pd.concat([grid, max_weights], axis=1)
    return results


def max_station_weight(weights, stations):
    """Calculate distance from closest station at each grid point"""
    # Pollutant names
    pollutants = get_pollutants(stations)

    # Pollutant-specific maximun distances
    max_weights = {}
    for pol in pollutants:
        pol_w = weights.copy()
        pol_w[:, pd.isnull(stations[pol]).values] = np.nan
        pol_max_w = np.nanmax(pol_w, axis=1)
        max_weights.update({pol: pol_max_w})
    return pd.DataFrame(max_weights)


def combine_station_weights(weight_dist, weight_lc, w_max=1.3):
    """
    Combine the weights determined by distance from station with weights from land-cover
    Combine the weights by: combined_weights = distance_weights*(land-cover_weights+1)
    Normalize by defined min and max values that allows modifying how sensetive the result is
    to land-cover weights
    Parameters
    ----------
    weight_dist : dataframe
        grid points weights determined by distance from stations per pollutants.
    weight_lc : dataframe
        grid points weights determined by land-cover.
    w_max : float, optional
        max value for normalization, the lower it is the less noticble changes in the
        land-cover will be. The default is 1.3.

    Returns
    -------
    combined_weights : dataframe

    """
    # Pollutant names
    pollutants = get_pollutants(weight_dist)

    # Pollutant-specific combined weight
    combined_weights = {}
    for pol in pollutants:
        w_pol = (
             weight_dist[pol].values +
             (weight_dist[pol].values * weight_lc['weight'].values)
             )
        w_pol = normalize_by_min_max(w_pol, data_min=0, data_max=w_max)
        w_pol = np.clip(w_pol, a_min=0, a_max=1)
        combined_weights.update({pol: w_pol.copy()})
    return pd.DataFrame(combined_weights)


def normalize_by_min_max(data, data_min=None, data_max=None):
    """Normalize data by min-max range"""
    if data_min is None:
        data_min = np.min(data)
    if data_max is None:
        data_max = np.max(data)
    return (data - data_min) / (data_max - data_min)


def min_station_distance(grid, stations):
    """
    Not in use anymore.
    Calculate distance from closest station at each grid point
    """
    # Distances
    distance = cartesian_distance(grid, stations)

    # Pollutant names
    pollutants = get_pollutants(stations)

    # Pollutant-specific minimum distances
    results = {}
    for pol in pollutants:
        pol_d = distance.copy()
        pol_d[:, pd.isnull(stations[pol]).values] = np.nan
        pol_min_d = np.nanmin(pol_d, axis=1)
        results.update({pol: pol_min_d})

    # Format results as DataFrame and append coordinates
    results = pd.concat((grid[['lat', 'lon']], pd.DataFrame(results)), axis=1)
    return results


def smoke_cams_weighter(smoke, cams):
    """Determine smoke data weights to use for stacking with CAMS"""
    pollutants = list(set(smoke.columns) - {'lat', 'lon'})
    weights = {}

    for pol in pollutants:
        if pol in cams.columns:
            # Stack the two models, assuming they are sorted by [lat, lon]
            data = np.stack((cams[pol].values, smoke[pol].values), axis=0)
            # Calculate the weights
            w = np.nanargmax(data, axis=0).astype(float)
        else:
            # In case CAMS is missing a pollutant
            w = np.ones((smoke[pol].values.shape), dtype=float)

        weights.update({pol: w})

    weights = pd.concat(
            [smoke[['lat', 'lon']], pd.DataFrame(weights).clip(0, 1)], axis=1)
    return weights
