import numpy as np
from scipy.spatial.distance import cdist

from .conversion import ll2xyz


def vincenty_distance(df_from, df_to):
    """Calculate Vincenty distances (in radians) between two dataframes"""
    # Coordinates
    lat_from, lat_to = np.radians(np.meshgrid(df_from.lat, df_to.lat))
    lon_from, lon_to = np.radians(np.meshgrid(df_from.lon, df_to.lon))

    delta_lat = np.abs(lon_to - lon_from)

    # Calculate distance
    numerator = np.sqrt(
        (np.cos(lat_to) * np.sin(delta_lat)) ** 2 +
        (np.cos(lat_from) * np.sin(lat_to) - np.sin(lat_from) * np.cos(lat_to) * np.cos(delta_lat)) ** 2)  # noqa: E501

    denominator = np.sin(lat_from) * np.sin(lat_to) + np.cos(lat_from) * np.cos(lat_to) * np.cos(delta_lat)  # noqa: E501

    distance = np.arctan2(numerator, denominator)
    return distance


def cartesian_distance(df_from, df_to):
    """Calculate Cartesian distances (in radians) between two dataframes"""
    # Cartesian coordinates
    xyz0 = ll2xyz(df_from.lat, df_from.lon)
    xyz1 = ll2xyz(df_to.lat, df_to.lon)

    # Calculate distance
    distance = cdist(xyz0, xyz1)            # Chord (Eucledian) distance
    distance = 2 * np.arcsin(distance / 2)  # Arc (actual) distance

    return distance
