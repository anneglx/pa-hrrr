import numpy as np

from scipy.interpolate import RectSphereBivariateSpline


class RSBSWrapper(RectSphereBivariateSpline):
    """
    scipy.interpolate.RectSphereBivariateSpline wrapper class

    Provides a similar interface to RectSphereBivariateSpline for (lat, lon)
    coordinates. Conversion to radians is handled in-class.

    To overcome a known limitation of FITPACK/SciPy where
    RectSphereBivariateSpline cannot be instantiated for a grid entirely
    within the Eastern Hemisphere, in such cases, all data is nudged 180
    degrees westward.

    Note that this means the wrapper cannot be safely used for extrapolating
    predictions (interpolating outside insantiation domain).

    TODO: Add further documentation, links to SciPy issues, examples
    """

    def __init__(self, lat, lon, r, s=0.0, pole_continuity=False,
                 pole_values=None, pole_exact=False, pole_flat=False):
        self._set_lon_offset(lon)
        u = self._lat2u(lat)
        v = self._lon2v(lon)
        return super().__init__(u, v, r, s, pole_continuity,
                                pole_values, pole_exact, pole_flat)

    def ev(self, lat, lon, dtheta=0, dphi=0):
        # theta = self._lat2u(lat)
        # phi = self._lon2v(lon)
        # return super().ev(theta, phi, dtheta, dphi)
        raise NotImplementedError()

    def __call__(self, lat, lon, dtheta=0, dphi=0, grid=True):
        theta = self._lat2u(lat)
        phi = self._lon2v(lon)

        # TODO: Raise DomainException when trying to interpolate outside initialization grid

        return super().__call__(theta, phi, dtheta, dphi, grid)

    def _lat2u(self, lat):
        """Convert latitudes to u (theta)"""
        return np.radians(lat + 90)

    def _lon2v(self, lon):
        """Convert longitudes to v (phi)"""
        return np.radians(lon + 180 + self.lon_offset)

    def _set_lon_offset(self, lon):
        """
        Set longitude offset if entire grid is on Eastern Hemisphere

        Workaround to known Scipy/FITPACK limitation: can not instantiate a
        RectSphereBivariateSpline instance where minimum longitude >= 0.
        """
        if min(lon) >= 0:
            self.lon_offset = -180
        else:
            self.lon_offset = 0


class SplineInterpolator(object):
    """Spline interpolator"""
    def __init__(self, df, pollutants):
        """Initialize instance"""
        self.pollutants = pollutants
        self.interpolators = self._init_interpolators(df)

    def _init_interpolators(self, df):
        """Initialize per-pollutant interpolators"""
        interpolators = {}
        for pollutant in self.pollutants:
            interpolators.update({
                pollutant: self._create_interpolator(df, pollutant)})

        return interpolators

    def __call__(self, grid, pollutant):
        """Interpolate over grid"""
        return self._interpolate(self.interpolators[pollutant], grid)

    def _create_interpolator(self, df, pollutant):
        """Create interpolator"""
        lat = np.unique(df['lat'])
        lon = np.unique(df['lon'])

        data = df.pivot(index='lat', columns='lon', values=pollutant).values

        return RSBSWrapper(lat, lon, data)

    def _interpolate(self, interpolator, grid):
        """Return result of interpolation over grid"""
        lat = grid.lat.values
        lon = grid.lon.values

        result = interpolator(lat, lon, grid=False)
        return result
