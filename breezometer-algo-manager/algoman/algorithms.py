import numpy as np
import pandas as pd

from .spatial import cartesian_distance
from .weights import inverse_distance_weight, occlusion_weight, lizard_eye
from .utils import filter_stations_for_pollutant, subset_weights, format_results, get_pollutants  # noqa: E501


def nearest_neighbor(grid, stations, max_distance):
    """Nearest-neighbor algorithm"""
    pollutants = get_pollutants(stations)
    distances = cartesian_distance(grid, stations)

    results = {}
    for pol in pollutants:
        # Filter stations to only those measuring current pollutant
        pol_stations_index = filter_stations_for_pollutant(stations, pol)
        if len(pol_stations_index):
            # Pollutant specific weights and concentrations
            pol_con = stations[pol][pol_stations_index].values

            # Result at each grid point taken directly from nearest station
            pol_dist = subset_weights(distances, pol_stations_index)
            pol_result = pol_con[np.argmin(pol_dist, axis=1)]

            # Nullify gridpoints with no nearby stations
            pol_min_dist = np.min(pol_dist, axis=1)
            try:
                # In case smoke was involved, max_distance is an array
                out_of_range = (pol_min_dist[:, np.newaxis]
                                > max_distance[np.newaxis, :]).all(axis=1)
                pol_result[out_of_range] = np.nan
            except (TypeError, IndexError):
                pol_result[pol_min_dist > max_distance] = np.nan
        else:
            # NaN if zero stations measure current pollutant
            pol_result = np.nan
        results.update({pol: pol_result})

    # Format results into DataFrame
    results = format_results(grid, results)

    # Handle stations directly on gridpoints
    ontop = pd.merge(grid, stations, on=['lat', 'lon'], right_index=True)
    results.update(ontop)

    return results


def IDW(grid, stations, max_distance):
    """Inverse Distance Weighting algorithm"""
    pollutants = get_pollutants(stations)
    distances = cartesian_distance(grid, stations)
    idw = inverse_distance_weight(distances, max_distance=max_distance)

    results = {}
    for pol in pollutants:
        # Filter stations to only those measuring current pollutant
        pol_stations_index = filter_stations_for_pollutant(stations, pol)
        if len(pol_stations_index):
            # Pollutant specific weights and concentrations
            pol_idw = subset_weights(idw, pol_stations_index)
            pol_con = stations[pol][pol_stations_index].values

            # Pollutant final weights
            pol_weights = pol_idw
            pol_totalweight = pol_weights.sum(axis=1)

            # Pollutant final grid
            pol_result = (pol_weights * pol_con).sum(axis=1)
            pol_result /= pol_totalweight

            # Nullify gridpoints with no nearby stations
            pol_dist = subset_weights(distances, pol_stations_index)
            pol_min_dist = np.min(pol_dist, axis=1)
            try:
                # In case smoke was involved, max_distance is an array
                out_of_range = (pol_min_dist[:, np.newaxis]
                                > max_distance[np.newaxis, :]).all(axis=1)
                pol_result[out_of_range] = np.nan
            except (TypeError, IndexError):
                pol_result[pol_min_dist > max_distance] = np.nan
        else:
            # NaN if zero stations measure current pollutant
            pol_result = np.nan
        results.update({pol: pol_result})

    # Format results into DataFrame
    results = format_results(grid, results)

    # Handle stations directly on gridpoints
    # ontop = pd.merge(grid, stations, on=['lat', 'lon'], right_index=True)
    ontop = pd.merge(grid, stations, on=['lat', 'lon'], how='left')
    ontop = ontop.dropna()

    results.update(ontop)

    return results


def ADW(grid, stations, max_distance):
    """Angle-Adjusted Distance Weighting algorithm"""
    pollutants = get_pollutants(stations)
    distances = cartesian_distance(grid, stations)
    idw = inverse_distance_weight(distances, max_distance=max_distance)
    ow = occlusion_weight(grid, stations)

    results = {}
    for pol in pollutants:
        # Filter stations to only those measuring current pollutant
        pol_stations_index = filter_stations_for_pollutant(stations, pol)
        if len(pol_stations_index):
            # Pollutant specific weights and concentrations
            pol_idw = subset_weights(idw, pol_stations_index)
            pol_ow = subset_weights(ow, pol_stations_index)
            pol_con = stations[pol][pol_stations_index].values

            # Pollutant final weights
            pol_weights = pol_idw * pol_ow.prod(axis=2)
            pol_totalweight = pol_weights.sum(axis=1)

            # Pollutant final grid
            pol_result = (pol_weights * pol_con).sum(axis=1)
            pol_result /= pol_totalweight

            # Nullify gridpoints with no nearby stations
            pol_dist = subset_weights(distances, pol_stations_index)
            pol_min_dist = np.min(pol_dist, axis=1)
            try:
                # In case smoke was involved, max_distance is an array
                out_of_range = (pol_min_dist[:, np.newaxis]
                                > max_distance[np.newaxis, :]).all(axis=1)
                pol_result[out_of_range] = np.nan
            except (TypeError, IndexError):
                pol_result[pol_min_dist > max_distance] = np.nan

        else:
            # NaN if zero stations measure current pollutant
            pol_result = np.nan
        results.update({pol: pol_result})

    # Format results into DataFrame
    results = format_results(grid, results)

    # Handle stations directly on gridpoints
    ontop = pd.merge(grid, stations, on=['lat', 'lon'], right_index=True)
    results.update(ontop)

    return results


def LEO(grid, stations, max_distance):
    """Lizard-Eye Occlusion algorithm"""
    pollutants = get_pollutants(stations)
    distances = cartesian_distance(grid, stations)
    idw = inverse_distance_weight(distances, max_distance=max_distance)
    ow = lizard_eye(grid, stations, distances)

    results = {}
    for pol in pollutants:
        # Filter stations to only those measuring current pollutant
        pol_stations_index = filter_stations_for_pollutant(stations, pol)
        if len(pol_stations_index):
            # Pollutant specific weights and concentrations
            pol_idw = subset_weights(idw, pol_stations_index)
            pol_ow = subset_weights(ow, pol_stations_index)
            pol_con = stations[pol][pol_stations_index].values

            # Pollutant final weights
            pol_weights = pol_idw * pol_ow.prod(axis=2)
            pol_totalweight = pol_weights.sum(axis=1)

            # Pollutant final grid
            pol_result = (pol_weights * pol_con).sum(axis=1)
            pol_result /= pol_totalweight

            # Nullify gridpoints with no nearby stations
            pol_dist = subset_weights(distances, pol_stations_index)
            pol_min_dist = np.min(pol_dist, axis=1)
            try:
                # In case smoke was involved, max_distance is an array
                out_of_range = (pol_min_dist[:, np.newaxis]
                                > max_distance[np.newaxis, :]).all(axis=1)
                pol_result[out_of_range] = np.nan
            except (TypeError, IndexError):
                pol_result[pol_min_dist > max_distance] = np.nan

        else:
            # NaN if zero stations measure current pollutant
            pol_result = np.nan
        results.update({pol: pol_result})

    # Format results into DataFrame
    results = format_results(grid, results)

    # Handle stations directly on gridpoints
    ontop = pd.merge(grid, stations, on=['lat', 'lon'], right_index=True)
    results.update(ontop)

    return results


ALGORITHMS = {
    'nearest_neighbor': nearest_neighbor,
    'idw': IDW,
    'adw': ADW,
    'leo': LEO
}
