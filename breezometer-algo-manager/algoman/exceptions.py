class OutOfBoundsError(Exception):
    """Value out of bounds exception"""
    pass


class UnknownAlgorithmError(Exception):
    """Unknown algorithm type exception"""
    pass


class MissingRequiredInputError(Exception):
    """Missing required input exception"""
    pass


class ManagerClassReuseError(Exception):
    """Manager class is being reused exception"""
    pass
