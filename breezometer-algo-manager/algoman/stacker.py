import numpy as np


def stack(cams_global=None, cams_regional=None,
          spatial_algo=None, smoke=None, station_weight=None,
          regional_cams_weight=None, smoke_cams_regional_weights=None,
          smoke_cams_global_weights=None):
    """Stack together the various algorithm layers"""
    # Note: The order of the first two inputs to ``smoosh_frames`` is important
    # Smoosh smoke and global CAMS
    stacked_g = smoosh_frames(smoke, cams_global, smoke_cams_global_weights)
    # Smoosh smoke and regional CAMS
    stacked_r = smoosh_frames(smoke, cams_regional, smoke_cams_regional_weights)  # noqa: E501
    # Smoosh all models together
    stacked = smoosh_frames(stacked_r, stacked_g, regional_cams_weight)
    # Smoosh combined models with spatial-algo
    stacked = smoosh_frames(spatial_algo, stacked, station_weight)

    return stacked


def smoosh_frames(df1, df2, weights):
    """Smoosh all layers of two dataframes together using provided weights"""
    if df1 is None and df2 is None:
        smooshed = None
    elif df1 is None:
        smooshed = df2.copy()
    elif df2 is None:
        smooshed = df1.copy()
    elif weights is None:
        # Happens in case weights don't exist but there is data in both layers.
        # Returning df2 instead.
        smooshed = df2.copy()
    else:
        # All pollutants
        pol_names1 = set(df1.drop(['lat', 'lon'], axis=1).columns)
        pol_names2 = set(df2.drop(['lat', 'lon'], axis=1).columns)
        pol_names = pol_names1.union(pol_names2)

        # Smoosh each layer
        smooshed = df1.copy()
        if 'weight' in weights.columns:
            for pollutant in pol_names:
                smooshed[pollutant] = smoosh_layers(
                    try_get_layer(df1, pollutant),
                    try_get_layer(df2, pollutant),
                    weights['weight'])
        else:
            for pollutant in pol_names:
                smooshed[pollutant] = smoosh_layers(
                    try_get_layer(df1, pollutant),
                    try_get_layer(df2, pollutant),
                    try_get_layer(weights, pollutant))

    return smooshed


def try_get_layer(df, key):
    """Try to retrieve layer from DataFrame, return None if failed"""
    try:
        layer = df[key]
    except KeyError:
        layer = None
    return layer


def smoosh_layers(layer1, layer2, weight_layer):
    """
    Smoosh two layers together using provided weight layer.

    Note that ``layer2`` is considered the "base", hence ``layer1`` is
    multiplied by ``weight`` and ``layer2`` by ``(1 - weight)``.
    """
    if layer1 is None and layer2 is None:
        smooshed = None
    elif layer1 is None:
        smooshed = layer2.copy()
    elif layer2 is None:
        smooshed = layer1.copy()
    elif weight_layer is None:
        # Happens in case weights don't exist while there is data in both
        # layers. Returning layer2 instead.
        smooshed = layer2.copy()
    else:
        if np.isnan(layer1).all():
            smooshed = layer2.copy()
        elif np.isnan(layer2).all():
            smooshed = layer1.copy()
        elif np.isnan(weight_layer).all():
            smooshed = layer2.copy()
        else:
            smooshed = np.nansum(
                (layer1 * weight_layer, layer2 * (1 - weight_layer)), axis=0)

    return smooshed
