import numpy as np
import pandas as pd
from scipy.interpolate import interpn
from scipy.ndimage.filters import percentile_filter, maximum_filter

from .utils import coords_inside_regional_domain, pivot_data
from .defaults import (SMOKE_CONC_BP, SMOKE_LEVELS_BP,
                       SMOKE_CALIBRATION_PARAMS, SMOKE_KERNEL_SIZES,
                       SMALLEST_MAXD_SMOKE, SMALLEST_MIND_SMOKE,
                       NO_LC_MAX_STATION_DISTANCE, NO_LC_MIN_STATION_DISTANCE)
from .constants import REGIONAL_SMOKE_BOUNDS


def can_use_regional_smoke(smoke, grid):
    """
    Return True if the grid is entirely contained in the regional smoke bbox.
    If not, return False.
    """
    return (smoke is not None
            and coords_inside_regional_domain(grid, model_name='smoke'))


def calibrate_smoke(smoke, model_type):
    """
    Calibrate a smoke model's data.

    Parameters
    ----------
    smoke: dataframe
        Must contain `lat`, `lon` and `pm25` columns.
    model_type: string {'global', 'regional'}
        To identify the type of model and determine which calibration
        parameters to use (see `defaults.py` for the default values).

    Returns
    -------
    dataframe similar to ``smoke`` with calibrated pm25 values
    """
    clip_max = SMOKE_CALIBRATION_PARAMS[model_type]['clip_max']
    scale_factor = SMOKE_CALIBRATION_PARAMS[model_type]['scale_factor']

    if clip_max is not None:
        smoke['pm25'] = np.clip(
                smoke['pm25'].values, a_min=None, a_max=clip_max)
    if scale_factor is not None:
        smoke['pm25'] = smoke['pm25'].values * scale_factor
    return smoke


def calc_stations_dynamic_distances(smoke_regional, smoke_global, stations,
                                    mind, maxd, distances_are_vectors=False):
    """
    Use fire smoke data to determine the area represented by stations.
    Will take only the relevant model (global/regional) into account per
    station, according to actual coverage of stations.

    Parameters
    ----------
    smoke_regional : dataframe
        Smoke model regional data. Must contain `lat`, `lon` and 'pm25'
        columns.
    smoke_global : dataframe
        Smoke model global data. Must contain `lat`, `lon` and 'pm25' columns.
    stations : dataframe of length L
        Stations data. Must contain `lat` and `lon` columns.
    mind : scalar or array of length L
        The minimum distance (in radians) to use for weighting when combining
        stations with other layers (CAMS, smoke).
        If scalar, will be treated as a uniform value for all stations;
        If vector, will be treated as a unique values per station.
    maxd : scalar or array of length L
        The maximum distance (in radians) to use for weighting when combining
        stations with other layers (CAMS, smoke).
        If scalar, will be treated as a uniform value for all stations;
        If vector, will be treated as a unique values per station.
    distances_are_vectors : bool
        A flag indicating whether the inputs `mind`, `maxd` are scalars (False)
        or vectors (True). The flag is set to True if land-cover weights are
        part of the input to the algo-manager. Default is False.

    Returns
    -------
    One of the following:

    * In case no stations were affected by smoke:
      Returns (mind, maxd) unchanged.
    * In case at least one station was affected by smoke:
      Returns two vector ndarrays of length L, holding the new minimum and
      maximum distances per-station after taking smoke into account.
    """
    stns_covered = stations_covered_by_smoke(
          smoke_regional, smoke_global, stations)

    # Calc only if there are any stations covered by smoke
    mind_reg, maxd_reg, mind_glob, maxd_glob = [], [], [], []
    if len(stns_covered['regional']):
        if distances_are_vectors:
            # In case mind/maxd are already vectors
            mind_reg, maxd_reg = vectorize_stations_distances(
                smoke_regional, stns_covered['regional'],
                mind[stns_covered['mask_reg']], maxd[stns_covered['mask_reg']],
                smoke_model_in_use='regional'
                )
        else:
            # In case mind/maxd are scalars
            mind_reg, maxd_reg = vectorize_stations_distances(
                smoke_regional, stns_covered['regional'],
                mind, maxd,
                smoke_model_in_use='regional'
                )

    if len(stns_covered['global']):
        if distances_are_vectors:
            mind_glob, maxd_glob = vectorize_stations_distances(
                smoke_global, stns_covered['global'],
                mind[stns_covered['mask_glob']], maxd[stns_covered['mask_glob']],
                smoke_model_in_use='global'
                )
        else:
            mind_glob, maxd_glob = vectorize_stations_distances(
                smoke_global, stns_covered['global'],
                mind, maxd,
                smoke_model_in_use='global'
                )

    if (len(mind_reg) > 0 or len(maxd_reg) > 0 or
            len(mind_glob) > 0 or len(maxd_glob) > 0):
        # Make sure distances are vectors
        if distances_are_vectors:
            mind_vec = mind
            maxd_vec = maxd
        else:
            # Will return 2 vectors anyway
            # Note: If the original distances are scalars it means there is no
            #       input of land-cover, and we should use the appropriate default
            mind_vec = np.full(len(stations), NO_LC_MIN_STATION_DISTANCE)
            maxd_vec = np.full(len(stations), NO_LC_MAX_STATION_DISTANCE)

        # Combine the distances' results
        mind_vec[stns_covered['mask_reg']] = mind_reg
        maxd_vec[stns_covered['mask_reg']] = maxd_reg
        mind_vec[stns_covered['mask_glob']] = mind_glob
        maxd_vec[stns_covered['mask_glob']] = maxd_glob

        return mind_vec, maxd_vec

    else:
        return mind, maxd


def stations_covered_by_smoke(smoke_regional, smoke_global, stations):
    """
    Check which smoke model to use for each station.

    Returns a dictionary with the following items for each model:

    * the relevant stations' data
    * a boolean mask to indicate where the stations' data was taken from
    """
    res = {'regional': [], 'global': [], 'mask_reg': [], 'mask_glob': []}

    if smoke_regional is None:
        if smoke_global is None:
            return res
        else:
            res['global'] = stations.copy()
            res['mask_glob'] = np.full(len(stations), True, dtype=bool)
            res['mask_reg'] = np.full(len(stations), False, dtype=bool)

    else:
        mask_reg = np.bitwise_and(
                np.bitwise_and(stations['lat'] >= REGIONAL_SMOKE_BOUNDS[1],
                               stations['lat'] <= REGIONAL_SMOKE_BOUNDS[3]),
                np.bitwise_and(stations['lon'] >= REGIONAL_SMOKE_BOUNDS[0],
                               stations['lon'] <= REGIONAL_SMOKE_BOUNDS[2]),
            )
        res['regional'] = stations[mask_reg].reset_index(drop=True)
        res['mask_reg'] = mask_reg.copy()

        if smoke_global is None:
            res['global'] = []
            res['mask_glob'] = np.full(len(stations), False, dtype=bool)
        else:
            res['global'] = stations[~mask_reg].reset_index(drop=True)
            res['mask_glob'] = ~mask_reg.copy()

    return res


def vectorize_stations_distances(smoke, stations, mind, maxd,
                                 smoke_model_in_use):
    """
    Use fire smoke data to determine the area represented by stations.

    In case a station is affected by fire smoke*, we assume it's measurements
    are representative of a smaller area, compared to ambient conditions.
    Hence, we calculate the "affectance factor" of a station, which ranges
    from 1 to 0: high when the smoke effect is strong and low when the effect
    is weak.
    We use the value of (1 - affectance**0.75) to scale the max and min
    distances of interpolation around each station.

    The final value of maxd_modified (mind_modified) will alwayls be in the
    range between maxd (mind) to SMALLEST_MAXD_SMOKE (SMALLEST_MIND_SMOKE).

    (*) Affectance is strong when a station is inside a smoke plume, or close
    to it from the outside.

    Parameters
    ----------
    smoke : dataframe
        Smoke model data. Must contain `lat`, `lon` and 'pm25' columns.
    stations : dataframe of length L
        Stations data. Must contain `lat` and `lon` columns.
    mind : scalar or array of length L
        The maximum distance (in radians) to use for weighting when combining
        stations with other layers (CAMS, smoke).
        If scalar, will be treated as a uniform value for all stations;
        If vector, will be treated as a unique values per station.
    maxd : scalar or array of length L
        The minimum distance (in radians) to use for weighting when combining
        stations with other layers (CAMS, smoke).
        If scalar, will be treated as a uniform value for all stations;
        If vector, will be treated as a unique values per station.
    smoke_model_in_use : str
        Which model are we using - "global" or "regional". This affects the
        kernel sizes used in later calculations.

    Equations
    ---------
    ::
        aff = smoke_levels / scale_by
        aff = aff.clip(max=max_aff) / max_aff
        shrink_ratio = 1 - (aff ** 0.75)

        maxd_modified = maxd * shrink_ratio + SMALLEST_MAXD_SMOKE * (1 - shrink_ratio)
        mind_modified = mind * shrink_ratio + SMALLEST_MIND_SMOKE * (1 - shrink_ratio)

    Example
    -------
        Say the default distances are:
        ::

            maxd = 200km, SMALLEST_MAXD_SMOKE = 30km
            mind = 10km, SMALLEST_MIND_SMOKE = 10km

        If a station is not affected by smoke, it’s affectence will be close
        to zero and maxd and mind will remain the default ones (200km, 10km).

        If a station is located where smoke levels are very high (i.e. 6)
        the affectance can get to its maximum value of 0.7 / 0.7 = 1,
        resulting in:
        ::

            maxd = 200km * (1 - (1 ** 0.75)) + 30km * (1 ** 0.75) = 30km
            mind = 10km * (1 - (1 ** 0.75)) + 10km * (1 ** 0.75) = 10km

    Returns
    -------
    Two vector ndarrays of length L, holding the new minimum and maximum
    distances after taking smoke into account.
    """
    # Prepare the data
    smoke_data, smoke_lat, smoke_lon = pivot_data(smoke)
    # Convert smoke concentrations to an "index" with values in the range 0-6
    smoke_levels = calc_smoke_levels(smoke_data)

    # Calculate affectance
    aff = get_smoke_affectance(
        smoke_levels, smoke_lat, smoke_lon, stations, smoke_model_in_use
        )

    # Scale the affectence and invert it to get the shrink ratio
    shrink_ratio = 1 - (aff ** 0.75)

    # Modify the distances (while converting scalar to vector)
    maxd_modified = maxd * shrink_ratio + SMALLEST_MAXD_SMOKE * (1 - shrink_ratio)
    mind_modified = mind * shrink_ratio + SMALLEST_MIND_SMOKE * (1 - shrink_ratio)

    # Shrink maxd of additional stations, if needed
    maxd_modified = extend_smoke_to_affect_more_stations(
        maxd_modified, smoke_levels, smoke_lat, smoke_lon,
        stations, smoke_model_in_use
        )
    return mind_modified, maxd_modified


def get_smoke_affectance(smoke_levels, smoke_lat, smoke_lon, stations,
                         smoke_model_in_use, scale_by=max(SMOKE_LEVELS_BP),
                         max_aff=0.7):
    """
    Use fire smoke data to determine the affectance of the smoke over stations.

    We "inflate" the smoke levels in order to include the smoke's influence
    on stations that are both inside plumes and next to plumes from the
    outside.
    Affectance ranges from 1 to 0: high when the smoke effect is strong and
    low when the effect is weak.

    The affectance is determined by the presence of the station in the
    image-filter kernels we use on fire smoke data.

    For more information, see the documentation for `filter_smoke_levels()`.

    Parameters
    ----------
    smoke : dataframe
        Smoke model data. Must contain `lat`, `lon` and 'pm25' columns.
    stations : dataframe of length L
        Stations data. Must contain `lat` and `lon` columns.
    smoke_model_in_use : str
        Which model are we using - "global" or "regional". This affects the
        kernel sizes used in the percentile filter.
    scale_by : int or float, optional, default is 6
        Scale factor for the affectance. Adjusts the range to be [0, 1].
    max_aff : int or float, optional, default is 0.7
        The highest affectance (``aff``) allowed.


    Example
    -------
    If the kernel size for the HRRR smoke model is 36, stations up 108 km will
    affected by it:
        affercted distance = kernel size * smoke model grid resolution
        108 = 36 * 3
        where 3 km is the HRRR grid resolution at the equator


    Returns
    -------
    One vector ndarray of length L, holding the affectance of smoke data on
    each station.
    """
    # "Blur" the smoke levels with an image filter to extend the plumes
    k_sizes = SMOKE_KERNEL_SIZES[smoke_model_in_use]
    smoke_levels_blur = filter_smoke_levels(
            smoke_levels,
            k_size=[k_sizes[0] * 2, k_sizes[1] * 2],
            percentile=[98, 95],
            filter_type='percentile'
            )
    # Get smoke levels at stations' locations
    smoke_levels_interp = interpolate_to_stations(
            smoke_levels_blur, smoke_lat, smoke_lon, stations)
    # Convert smoke levels to affectance by scaling
    aff = smoke_levels_interp.flatten() / scale_by
    aff = aff.clip(max=max_aff) / max_aff
    return aff


def extend_smoke_to_affect_more_stations(maxd, smoke_levels, smoke_lat,
                                         smoke_lon, stations, smoke_model_in_use,
                                         threshold=1):
    """
    Extend the smoke data's effect even further to prevent artifacts on the map.

    This function is meant to be called with a very large kernel size
    (e.g. 72 * 2 for HRRR). The large kernel should cover the defualt maximum
    distance (e.g. 200km) in order to include all stations in that range.

    All stations in the range on the kernel will get a reduced maximum
    distance, which is equal to NO_LC_MAX_STATION_DISTANCE (e.g. 100km).

    This is done in order to prevent weird artifacts, caused by large gaps
    between stations with very small maximum distance (e.g. 30km) and other
    stations with very large maximum distance (e.g. 200km).


    Parameters
    ----------
    maxd : ndarray vactor
        stations max interpolation distance, in vector form for each station.
    smoke_levels : ndarray
        Smoke levels represent the intensity/density of the smoke in each pixel
        of the model.
    smoke_lat : ndarray vactor
        vector of the latitude values of the smoke level matrix.
    smoke_lon : ndarray vactor
        vector of the longitude values of the smoke level matrix..
    stations : dataframe of length L
        Stations data. Must contain `lat` and `lon` columns.
    smoke_model_in_use : str
        Which model are we using - "global" or "regional". This affects the
        kernel sizes used in the percentile filter.
    threshold : int or float, default is 1.
        Define smoke levels that are very low so that the
        NO_LC_MAX_STATION_DISTANCE wont be enforced in areas with no
        significant fire.

    Returns
    -------
    maxd : ndarray vactor
        Stations max interpolation distance, in vector form of length L.
        It is the minimum value of the maxd-input and the maxd calculted in
        this function.

    """
    k_sizes = SMOKE_KERNEL_SIZES[smoke_model_in_use]
    # Calculate new smoke levels by the max value in the kernel
    smoke_levels_blur = filter_smoke_levels(
                smoke_levels,
                k_size=[k_sizes[2] * 2],
                filter_type='max'
                )
    smoke_levels_interp = interpolate_to_stations(
            smoke_levels_blur, smoke_lat, smoke_lon, stations)

    # Find all relevant stations and change their maxd
    maxd_additionals = maxd.copy()
    maxd_additionals[smoke_levels_interp > threshold] = NO_LC_MAX_STATION_DISTANCE

    # Take the smallest maxd for each station
    maxd = np.amin([maxd, maxd_additionals], axis=0)
    return maxd


def calc_smoke_levels(smoke_data, conc_bp=SMOKE_CONC_BP,
                      levels_bp=SMOKE_LEVELS_BP):
    """
    Convert smoke concentrations to an "index" with values in the range 0-6.

    Smoke levels represent the intensity/density of the smoke in each pixel
    of the model. Levels are obtained by converting the smoke concentration
    to an "index" using threshold values (e.g. concentration between 21 and
    50 will get an index between 2 and 2.9).
    """
    smoke_levels = np.full_like(smoke_data, np.nan)

    for i in range(len(conc_bp) - 1):
        conc_bp_low = conc_bp[i]
        conc_bp_high = conc_bp[i + 1]

        levels_bp_low = levels_bp[i]
        levels_bp_high = levels_bp[i + 1]

        index = np.bitwise_and(
            smoke_data >= conc_bp_low,
            smoke_data < conc_bp_high
        )

        smoke_levels[index] = ((
            (smoke_data[index] - conc_bp_low) / (conc_bp_high - conc_bp_low))
            * (levels_bp_high - levels_bp_low) + levels_bp_low
        )

    smoke_levels[smoke_data >= conc_bp[-1]] = levels_bp[-1]

    return smoke_levels


def filter_smoke_levels(smoke_levels, k_size=[18 * 2, 9 * 2],
                        filter_type='percentile', percentile=[98, 95]):
    """
    Use an image filter to smooth and "inflate" the smoke levels around
    the plumes.

    We "inflate" the smoke levels in order to include the smoke's influence
    on stations that are both inside plumes and next to plumes from the
    outside.

    This function is meant to be called twice: (i) to calculate two variations
    of percentile filter, (ii) to calculate maximum filter once with a very
    large kernel.

    The default flow with filter_type='percentile':
        1. Filter smoke levels with a large kernel and high percentile
        2. Filter smoke levels with a smaller kernel and lower percentile
        3. At each point take the maximum value from the filter results.
           We use maximum to keep the values of smoke levels in their original
           range, as opposed to - for example - adding them together.
           These are the new smoke levels.

    The default flow with filter_type='max':
        1. Filter smoke levels with a large kernel, taking the maximum value
           of the environment.
        2. At each point take the maximum value from the filter results.

    Parameters
    ----------
    smoke_levels : ndarray of floats
        Smoke levels values in "short-form", e.g. shape=(# lat, # lon).
    k_size : list of ints or floats
        List of length 2 with the kernel sizes to use for filtering.
        The kernel size is its diameter, hence the defaults are the radius
        multiplied by 2 (i.e. 18 * 2).
    filter_type : str
        Which image filter to use: maximum / percentile. Default is "percentile".
    percentile : list of ints or floats
        List of length 2 with the percentile values to use for filtering.

    Returns
    -------
    ndarray with the filtered data, of the same shape as smoke_levels.

    About percentile filters
    ------------------------
    The p-percentile filter for a scalar discrete image: in each neighborhood
    Nx centered at position x calculate the p-percentile value of all pixel
    values in that neighborhood. That percentile value becomes the result of
    the median filter at position x. Repeat this for all neighborhoods,
    i.e. for all points x in the image.

    For more info:
    https://docs.scipy.org/doc/scipy/reference/generated/scipy.ndimage.percentile_filter.html#scipy.ndimage.percentile_filter
    https://staff.fnwi.uva.nl/r.vandenboomgaard/IPCV20172018/LectureNotes/IP/LocalOperators/percentilefilter.html
    """
    smoke_levels_blur = []
    for i in range(len(k_size)):
        ks = k_size[i]
        disk_mask = get_disk_mask(ks)

        if filter_type == 'percentile':
            p = percentile[i]
            smoke_levels_blur.append(
                    percentile_filter(smoke_levels, percentile=p, footprint=disk_mask)
                    )
        elif filter_type == 'max':
            smoke_levels_blur.append(
                    maximum_filter(smoke_levels, footprint=disk_mask)
                    )

    return np.nanmax(np.stack(smoke_levels_blur, axis=0), axis=0)


def get_disk_mask(n):
    """Make a round footprint for an image filter"""
    a, b = n / 2, n / 2
    nx, ny = n, n
    y, x = np.ogrid[-a:nx-a, -b:ny-b]  # make "meshgrid" coordinates
    # Use logic with the circle equation to get a "round" boolean mask
    return x * x + y * y <= (((n / 2) - 1) * ((n / 2) - 1))


def interpolate_to_stations(model_data, model_lat, model_lon, target_points,
                            method='linear'):
    """Interpolate to stations locations"""
    interp_res = interpn(points=(model_lat, model_lon),
                         values=model_data,
                         xi=target_points[['lat', 'lon']].values,
                         method=method,
                         bounds_error=False,
                         fill_value=0.0)
    return interp_res
