import numpy as np

from .layer_weighters import (regional_cams_weighter,
                              stations_weighter,
                              station_distance_weighter,
                              smoke_cams_weighter,
                              adjust_station_max_distance_in_EU,
                              adjust_max_distance_in_stations_w_high_conc)
from .exceptions import MissingRequiredInputError, ManagerClassReuseError
from .utils import coords_outside_regional_domain
from .algorithms import ALGORITHMS
from .grid_interpolation import interpolate_grid2grid
from .stacker import stack
from .smoke import (can_use_regional_smoke,
                    calibrate_smoke,
                    calc_stations_dynamic_distances)
from . import defaults


class Manager(object):
    """
    Perform spatial interpolations to estimate air pollution concentrations
    over a grid, using various input data types.

    The `Manager` class utilizes stations (air quality monitors) measurements,
    CAMS models data, and traffic-derived pollutant concentration deltas, to
    estimate air pollutants concentrations over a provided grid.

    Parameters
    ----------
    grid : dataframe
        Coordinates in which to calculate results. Must contain `lat` and `lon`
        columns. Note that if performing CAMS interpolation, this must be a
        regular grid.
    stations : dataframe, optional
        Air quality monitoring stations to be used in `spatial_algo`
        calculations. Must contain `lat` and `lon` columns, as well as a column
        for each pollutant.
    traffic : dataframe, optional
        Traffic-derived concentration deltas to add on top of CAMS and station
        based data. Must contain `lat` and `lon` columns, as well as a column
        for each pollutant. Also, index and coordinates must be *identical* to
        `grid`.
    cams_global : dataframe, optional
        Global CAMS model data. Must contain `lat` and `lon` columns, as well
        as a column for each pollutant.
    cams_regional : dataframe, optional
        Regional CAMS (extended Europe) model data. Must contain `lat` and
        `lon` columns, as well as a column for each pollutant.
    bosch_kludge_data : dataframe, optional
        Stations derived concentrations to replace in the specific grid points
        this is used to "fix" a problem with traffic/traffic fixes.
        DATA IN THIS DATAFRAME WILL REPLACE IN ORDER ALL OTHER DATA
        AND OVERRIDES EVERYTHING
        Also, index and coordinates must be *identical* to
        `grid`.
    smoke_global : dataframe, optional
        Global fire-smoke model data. Must contain `lat`, `lon` and `pm25`
        columns.
    smoke_regional : dataframe, optional
        Regional fire-smoke (US CONUS) model data. Must contain `lat`, `lon`
        and `pm25` columns. The model's  full domain has to have a rectangular
        shape.
    land_cover_weights : dataframe, optional
        Weights from Land cover. Must contain 'lat', 'lon' and 'weight'

    Returns
    -------
    final : dataframe
        Final, unified result of all spatial calculations conducted on top of
        `grid`.

    Other Parameters
    ----------------
    min_station_distance : int or float
        The distance, in radians, from which a station's weight starts
        linearily decreasing (in favor of backdrop CAMS data). Defaults to 30
        kilometers (expressed in radians).
    max_station_distance : int or float
        The distance, in radians, up to which a station's data is used in
        spatial calculations. Defaults to 100 kilometers (expressed in
        radians).
    spatial_algo : {'leo', 'adw', 'idw', 'nearest_neighbor'}
        Spatial algorithm to be used for interpolating station data. Defaults
        to 'leo'. See specific algorithm documentation in `algoman.algorithms`.
    cams_intrp_method : {'pwise', 'rsbs', 'linear'}
        Interpolation method to be used for CAMS data. Defaults to 'pwise'. See
        specific documentation in `algoman.cams`.
    smoke_intrp_method : {'pwise', 'rsbs', 'linear'}
        Interpolation method to be used for smoke data. Defaults to 'pwise'.
        See specific documentation in `algoman.cams`.
    smoke_pm10_factor : int or float
        Factor to use as multiplier for creating PM10 from PM2.5 smoke data.
        Defaults to 1.5.
    """

    def __init__(self, grid, stations=None, traffic=None,
                 cams_global=None, cams_regional=None,
                 bosch_kludge_override_data=None,  # BOSCH_KLUDGE
                 smoke_regional=None, smoke_global=None,
                 land_cover_weights=None, **kwargs):

        self.grid = grid.copy() if grid is not None else None
        self.stations = stations.copy() if stations is not None else None
        self.traffic = traffic.copy() if traffic is not None else None
        self.cams_global = cams_global.copy() if cams_global is not None else None
        self.cams_regional = cams_regional.copy() if cams_regional is not None else None
        self.smoke_regional = smoke_regional.copy() if smoke_regional is not None else None
        self.smoke_global = smoke_global.copy() if smoke_global is not None else None
        self.land_cover_weights = land_cover_weights.copy() if land_cover_weights is not None else None

        # BOSCH_KLUDGE
        self.bosch_kludge_override_grid_data = bosch_kludge_override_data.copy() if bosch_kludge_override_data is not None else None

        # Override defaults
        self.min_station_distance = kwargs.get('min_station_distance', defaults.MIN_STATION_DISTANCE)  # noqa: E501
        self.max_station_distance = kwargs.get('max_station_distance', defaults.MAX_STATION_DISTANCE)  # noqa: E501
        self.spatial_algo = kwargs.get('spatial_algo', defaults.SPATIAL_ALGO)
        self.cams_intrp_method = kwargs.get('cams_intrp_method', defaults.CAMS_INTRP_METHOD)  # noqa: E501
        self.smoke_intrp_method = kwargs.get('smoke_intrp_method', defaults.SMOKE_INTRP_METHOD)  # noqa: E501
        self.smoke_pm10_factor = kwargs.get('smoke_pm10_factor', defaults.SMOKE_PM10_FACTOR)  # noqa: E501

        # Modify max stations distance according to land-cover input
        if self.stations is not None:
            if self.land_cover_weights is None:
                self.min_station_distance = defaults.NO_LC_MIN_STATION_DISTANCE
                self.max_station_distance = defaults.NO_LC_MAX_STATION_DISTANCE

        # Verify integrity of required parameters
        self.verify_integrity()

        # Has this class been called before?
        self.class_was_called = False

    def verify_integrity(self):
        """Verify integrity of required parameters"""
        # Fail if missing grid or spatial_algo
        if self.grid is None:
            raise MissingRequiredInputError('grid')
        if self.spatial_algo is None:
            raise MissingRequiredInputError('spatial_algo')

        # Fail if there's CAMS data but no interpolation method
        if (self.cams_global is not None) or (self.cams_regional is not None):
            if self.cams_intrp_method is None:
                raise MissingRequiredInputError('cams_intrp_method')

    def __call__(self):
        """Run algorithms and stack results"""
        if self.class_was_called:
            raise ManagerClassReuseError(
                    'Please init the class before calling')
        self.class_was_called = True

        self.run_algos()
        self.run_layer_weighters()
        self.stack_algos()

        if self.stacked is None:
            self.final = self.grid.copy()
        else:
            self.final = self.stacked.copy()
            if self.traffic is not None:
                self.overlay_traffic()
            # Calibrate PM25-PM10
            self.calibrate_pm()
            # BOSCH_KLUDGE
            if self.bosch_kludge_override_grid_data is not None:
                self.bosch_kludge_override_grid()

        self.handle_negatives()
        return self.final

    def overlay_traffic(self):
        """Add traffic-derived concentration deltas to final result grid"""
        # Fetch relevant pollutants - only those appearing in both DataFrames
        traffic_pollutants = set(self.traffic.columns) - {'lat', 'lon'}
        final_pollutants = set(self.final.columns) - {'lat', 'lon'}
        pollutants = traffic_pollutants & final_pollutants

        if len(self.grid) == 1:
            # Special case for leave-one-out functionality - handles instances
            # where `grid` is comprised of a single point, not necessarily
            # aligned with traffic grid.

            # Find nearest traffic grid point
            delta_lons = self.traffic.lon - self.grid.lon[0]
            delta_lats = self.traffic.lat - self.grid.lat[0]
            idx = ((delta_lons ** 2) + (delta_lats ** 2)).idxmin()

            for pol in pollutants:
                if not np.isnan(self.traffic[pol][idx]):
                    self.final[pol] += self.traffic[pol][idx]
        else:
            # For each pollutant, add deltas (making sure not to mess up NaNs)
            for pol in pollutants:
                # Locate non-NaN indexes for traffic data, and add deltas to final
                idx = self.traffic[pol].notnull()
                self.final[pol][idx] += self.traffic[pol][idx]

    def handle_negatives(self):
        """Round to zero negative values (can appear in CAMS interpolation)"""
        # NOTE: Assumes only columns are lat, lon, and pollutants
        pollutants = list(set(self.final.columns) - {'lat', 'lon'})
        self.final[pollutants] = self.final[pollutants].clip(lower=0)

    def run_algos(self):
        """Run all algorithms (CAMS, smoke and spatial)"""
        results = {'cams_global': None,
                   'cams_regional': None,
                   'smoke': None,
                   'spatial_algo': None}

        # CAMS
        if self.cams_global is not None:
            results.update({'cams_global': interpolate_grid2grid(
                self.grid, self.cams_global, self.cams_intrp_method)})

        if self.cams_regional is not None:
            if not coords_outside_regional_domain(self.grid, model_name='cams'):
                results.update({'cams_regional': interpolate_grid2grid(
                    self.grid, self.cams_regional, self.cams_intrp_method)})

        # Modify stations max distance
        distances_are_vectors = False
        if self.stations is not None:
            if self.land_cover_weights is not None:

                # Inside CAMS regional bbox
                self.min_station_distance, self.max_station_distance = (
                    adjust_station_max_distance_in_EU(
                        self.min_station_distance,
                        self.max_station_distance,
                        defaults.EU_MAX_STATION_DISTANCE,
                        self.stations
                        )
                    )
                distances_are_vectors = True

                # For stations with very high cocnentrations
                self.max_station_distance = (
                    adjust_max_distance_in_stations_w_high_conc(
                        self.max_station_distance,
                        defaults.HIGH_CONC_MAX_STATION_DISTANCE,
                        self.stations,
                        defaults.HIGH_CONC_THRESHOLDS
                        )
                    )

        # Smoke
        if self.smoke_regional is not None or self.smoke_global is not None:

            # (1)
            # Use smoke data as pollution-values to be added to the grid result
            if self.smoke_regional is not None:
                # Calibrate
                self.smoke_regional = calibrate_smoke(
                        self.smoke_regional, 'regional')
                if can_use_regional_smoke(self.smoke_regional, self.grid):
                    # Add Smoke PM10
                    # self.smoke_regional['pm10'] = (
                    #     self.smoke_pm10_factor * self.smoke_regional['pm25'])
                    # Interpolate
                    results.update({'smoke': interpolate_grid2grid(
                        self.grid, self.smoke_regional,
                        self.smoke_intrp_method, pwise_extrapolate=False)})

            if self.smoke_global is not None:
                # Calibrate
                self.smoke_global = calibrate_smoke(
                        self.smoke_global, 'global')
                # Only if global is the only smoke we can use for the grid:
                if not can_use_regional_smoke(self.smoke_regional, self.grid):
                    # Add Smoke PM10
                    # self.smoke_global['pm10'] = (
                    #     self.smoke_pm10_factor * self.smoke_global['pm25'])
                    # Interpolate
                    results.update({'smoke': interpolate_grid2grid(
                        self.grid, self.smoke_global, self.smoke_intrp_method,
                        pwise_extrapolate=False)})

            # (2)
            # Stations dynamic max distance
            if self.stations is not None:
                self.min_station_distance, self.max_station_distance = (
                        calc_stations_dynamic_distances(
                                self.smoke_regional,
                                self.smoke_global,
                                self.stations,
                                self.min_station_distance,
                                self.max_station_distance,
                                distances_are_vectors
                                )
                        )
        # Land cover weights interpolation
        if self.stations is not None:
            if self.land_cover_weights is not None:
                self.land_cover_weights = interpolate_grid2grid(
                    self.grid, self.land_cover_weights, method='linear')  # IDEA: maybe we can do the interpolation to our grid outside the algo, it creates dependency but might save run time

        # Spatial algo
        if self.stations is not None:
            results.update({'spatial_algo': ALGORITHMS[self.spatial_algo](self.grid, self.stations, self.max_station_distance)})  # noqa: E501
        self.results = results

    def run_layer_weighters(self):
        """Run appropriate weighters for selected algorithms"""
        weights = {}

        # Regioanl CAMS / Global CAMS
        weights.update({'regional_cams_weight': regional_cams_weighter(self.grid)})  # noqa: E501

        # Smoke / CAMS
        if self.results['smoke'] is not None:
            if self.results['cams_global'] is not None:
                self.smoke_cams_global_weight = smoke_cams_weighter(
                        self.results['smoke'].copy(),
                        self.results['cams_global'].copy()
                        )
                weights.update({'smoke_cams_global_weights': self.smoke_cams_global_weight})

            if self.results['cams_regional'] is not None:
                self.smoke_cams_regional_weight = smoke_cams_weighter(
                        self.results['smoke'].copy(),
                        self.results['cams_regional'].copy()
                        )
                weights.update({'smoke_cams_regional_weights': self.smoke_cams_regional_weight})

        # CAMS / Spatial algo
        if self.stations is not None:
            # Land-cover
            if self.land_cover_weights is not None:
                # Use combined weights (based on both distance and LC)
                weights.update(
                    {'station_weight': stations_weighter(
                        self.grid, self.stations, self.min_station_distance,
                        self.max_station_distance, weight_lc=self.land_cover_weights
                        )}
                    )
            # Only stations
            else:
                weights.update(
                    {'station_weight': station_distance_weighter(
                        self.grid, self.stations, self.min_station_distance,
                        self.max_station_distance
                        )}
                    )

        self.weights = weights


    def stack_algos(self):
        """Stack algorithm results into one layer"""
        temp_dict = dict(**self.results)
        temp_dict.update(self.weights)
        stacked = stack(**temp_dict)
        self.stacked = stacked

    def calibrate_pm(self):
        """In grid points where pm25 > pm10, enforce pm10 equal to pm25"""
        mask = self.final['pm25'] > self.final['pm10']
        self.final.loc[mask, 'pm10'] = self.final.loc[mask, 'pm25']

    # BOSCH_KLUDGE
    def bosch_kludge_override_grid(self):
        # Fetch relevant pollutants - only those appearing in both DataFrames
        traffic_pollutants = (
                set(self.bosch_kludge_override_grid_data.columns) -
                {'lat', 'lon'}
        )
        final_pollutants = set(self.final.columns) - {'lat', 'lon'}
        pollutants = traffic_pollutants & final_pollutants
        if len(self.grid) > 1:
            for pol in pollutants:
                idx = self.bosch_kludge_override_grid_data[pol].notnull()
                # this is done differently then the way that the traffic does
                # this as __setattr__ is treated differently then += by pandas
                # and the data is not updated correctly
                self.final.loc[idx, pol] = self.bosch_kludge_override_grid_data.loc[idx, pol]
