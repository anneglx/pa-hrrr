import numpy as np

from .exceptions import OutOfBoundsError


def ll2xyz(lat, lon):
    """Convert coordinates from geodesic (lat, lon) to Cartesian (x, y, z)"""
    theta = np.radians(90 - lat)
    phi = np.radians(lon)

    x = np.sin(theta) * np.cos(phi)
    y = np.sin(theta) * np.sin(phi)
    z = np.cos(theta)

    return np.vstack((x, y, z)).T


def lat2phi(lat):
    """Normalize latitudes from [-90, 90] to [0, pi]"""
    if (np.nanmin(lat) < -90) or (np.nanmax(lat) > 90):
        raise OutOfBoundsError('Latitude out of bounds [-90, 90]:', lat)
    return np.radians(lat + 90)


def lon2theta(lon):
    """Normalize longitudes from [-180, 180] to [-pi, pi]"""
    if (np.nanmin(lon) < -180) or (np.nanmax(lon) > 180):
        raise OutOfBoundsError('Longitude out of bounds [-180, 180]:', lon)
    return np.radians(lon)


# Unit conversions
ODIM = 111319.5  # One degree in meters at equator

def r2d(r):   return np.degrees(r)  # noqa: E272, E302
def d2r(d):   return np.radians(d)  # noqa: E272, E302
def m2km(m):  return m / 1000       # noqa: E272, E302
def km2m(km): return km * 1000      # noqa: E272, E302
def m2d(m):   return m / ODIM       # noqa: E272, E302
def d2m(d):   return d * ODIM       # noqa: E272, E302
def m2r(m):   return d2r(m2d(m))    # noqa: E272, E302
def r2m(r):   return d2m(r2d(r))    # noqa: E272, E302
def km2d(km): return m2d(km2m(km))  # noqa: E272, E302
def d2km(d):  return m2km(d2m(d))   # noqa: E272, E302
def km2r(km): return d2r(km2d(km))  # noqa: E272, E302
def r2km(r):  return d2km(r2d(r))   # noqa: E272, E302
