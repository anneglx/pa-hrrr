import pickle
from sklearn.cluster import AgglomerativeClustering
import pandas as pd


def cluster_pa(df, distance_threshold=0.05, minimal_n=2):
    """
    clusters purple-air sensors information using agglomerative clustering
    :param df:dataframe, required columns are lon, lat, pm25 and device_id
    :param distance_threshold: float
    :param minimal_n: int
    :return: pd.DataFrame
    """
    clustered = AgglomerativeClustering(distance_threshold=distance_threshold, n_clusters=None, linkage='complete').fit(df[['lat', 'lon']])
    df['CLUSTER_LABEL'] = clustered.labels_
    grouped = df.groupby(['CLUSTER_LABEL', 'datetime']).agg(
        lon=pd.NamedAgg(column="lon", aggfunc="median"),
        lat=pd.NamedAgg(column="lat", aggfunc="median"),
        pm25=pd.NamedAgg(column="pm25", aggfunc="median"),
        n=pd.NamedAgg(column="device_id", aggfunc="count")).reset_index()
    grouped = grouped[grouped.n >= minimal_n][['lon', 'lat', 'pm25', 'datetime']].reset_index()  # remove small clusters
    grouped.rename(columns={'datetime': 'dt'}, inplace=True)
    return grouped
