import numpy as np

from .conversion import ll2xyz


def inverse_distance_weight(distances, alpha=2, max_distance=None, distance_limited=True):  # noqa: E501
    """Inverse distance weighting with optional max_distance limiting"""
    weights = 1 / (distances ** alpha)

    if max_distance is not None:
        # Distance-limit weights
        if distance_limited:
            weights -= 1 / (max_distance ** alpha)

        # Zero out weights for stations outside of maximum range
        weights *= (distances <= max_distance)

    elif distance_limited:
        raise ValueError('max_distance can not be None if distance_limited is True!')  # noqa: E501

    return weights


def occlusion_weight(grid, stations):
    """Occlusion weighting"""
    # Grid and stations Cartesian coordinates
    g_xyz = ll2xyz(grid.lat, grid.lon)
    s_xyz = ll2xyz(stations.lat, stations.lon)

    # Station-gridpoint and station-station cross products (normalized)
    sg_cp = np.ndarray([s_xyz.shape[0], g_xyz.shape[0], 3])
    ss_cp = np.ndarray([s_xyz.shape[0], s_xyz.shape[0], 3])
    for i, si in enumerate(s_xyz):
        sg_cp[i] = np.cross(si, g_xyz)
        ss_cp[i] = np.cross(si, s_xyz)
    sg_cp /= np.linalg.norm(sg_cp, axis=2, keepdims=True)
    ss_cp /= np.linalg.norm(ss_cp, axis=2, keepdims=True)

    # Gridpoint-station-station pi-minus-theta
    gss_pmt = np.ndarray([g_xyz.shape[0], s_xyz.shape[0], s_xyz.shape[0]])
    for i in range(s_xyz.shape[0]):
        gss_pmt[:, :, i] = np.arccos(np.dot(sg_cp[i], ss_cp[i].T).clip(-1, 1))

    # Convert from theta to (pi - theta)
    gss_pmt = np.pi - gss_pmt

    # Change diagonal from NaN to 1
    for i in range(gss_pmt.shape[1]):
        gss_pmt[:, i, i] = 1

    return gss_pmt


def lizard_eye(grid, stations, distances):
    """Lizard-eye occlusion weighting"""
    def norm_cross_all_pairs(a, b):
        """
        Normalized cross product for all pairs of vectors in arrays a, b

        For `a` and `b` such that `a.shape == (n, x)` and `b.shape == (m, x)`,
        returns `a_cross_b` such that `a_cross_b.shape == (n, m, x)` and
        `a_cross_b[i, j] = np.cross(a[i], b[j]) / np.linalg.norm(np.cross(a[i], b[j]))`
        """
        a_cross_b = np.cross(a[:, None], b[None])
        a_cross_b /= np.linalg.norm(a_cross_b, axis=-1, keepdims=True)
        return a_cross_b

    def angle_aOb(x):
        """
        Angle ∠AOB between all points A and all points B through all points O

        For a given array `x` of vector cross-products where
        `x.shape == (m, n, 3)`, this function returns `c` the angles from all
        points `n` to all points `n` through all points `m`, such that
        `c.shape == (m, n, n)`
        """
        c = np.arccos(np.array([np.dot(p, p.T) for p in x]).clip(-1, 1))
        return c

    # Grid and stations Cartesian coordinates
    g_xyz = ll2xyz(grid.lat, grid.lon)
    s_xyz = ll2xyz(stations.lat, stations.lon)

    # Cross-products and angles
    g_cross_s = norm_cross_all_pairs(g_xyz, s_xyz)  # (g, s, 3)
    gss_angles = angle_aOb(g_cross_s)  # (g, s, s)
    # `gss_angles` above represents the angles between every grid and station
    # through every station. The lines below are there for memory conservation
    # reasons. See note below.
    gss_angles /= (np.pi/2)
    gss_angles.clip(0, 1, out=gss_angles)

    # Distance ratio between pairs of stations
    distance_ratio = distances[:, None, :] / distances[:, :, None]  # (g, s, s)
    # `distance_ratio` above represents the distance ratios between all
    # grid-station pairs. The lines below are there for memory conservation
    # reasons. See note below.
    distance_ratio *= -1
    distance_ratio += 1
    distance_ratio.clip(0, 1, out=distance_ratio)

    """
    NOTE: If we ignore the transformations on `gss_angles` and `distance_ratio`
    that appear above, this would be the actual algorithm:
    ```
    x0 = (gss_angles / (np.pi/2)).clip(0, 1)
    x1 = (1 - distance_ratio).clip(0, 1)
    ow = x0 ** x1
    ```
    However, to conserve memory, we transform the original arrays themselves
    and avoid creating new ones.
    """

    ow = gss_angles ** distance_ratio
    return ow
