# All values in degrees                                 # Original values

# Regional CAMS constants
REGIONAL_CAMS_BUFFER = 3
REGIONAL_CAMS_CENTER = (50, 10)
REGIONAL_CAMS_RADIUS = (19.95, 34.95)                   # (20, 35)
REGIONAL_CAMS_BOUNDS = (-24.95, 30.05, 44.95, 69.95)    # (-25, 30, 45, 70)

# Smoke constants
REGIONAL_SMOKE_BOUNDS = (
    -134.1079, 13.2921, -60.9109, 60.4611               # (-134.1079, 21.1, -60.9109, 52.6)
    )
