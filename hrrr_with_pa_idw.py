import numpy as np
import pandas as pd
import json
from datetime import datetime as dt
import numpy as np
import pandas as pd
import cv2
from scipy.interpolate import interpn
import matplotlib.pyplot as plt
plt.rcParams.update({'font.size': 14})
import matplotlib
matplotlib.use('tkagg')
from algoman.algorithms import IDW
from algoman.conversion import d2r
from algoman.layer_weighters import station_distance_weighter
from algoman.stacker import smoosh_frames


def load_files(hrrr_path, pa_nearest_gp_path):
    '''
    load and prep data.

    hrrr_path: str
        full path and file name of the hrrr file from algo input
    pa_nearest_gp_path : str
        full path and file name of the pa data, after adding the nearest hrrr gp coordinates
    '''
    # load hrrr
    j = json.load(open(f'{hrrr_path}'))
    df_hrrr_data = pd.DataFrame(j['values'])
    hrrr_metadata_dict = j['metadata'] # to be used before rapping back to json
    original_hrrr_cols = df_hrrr_data.columns
    df_hrrr_data['hrrr_pm25'] = pd.json_normalize(df_hrrr_data['pollutants'])
    df_hrrr_data = df_hrrr_data.drop(columns='pollutants')

    # load pa
    df_nearest_gp = pd.read_pickle(f'{pa_nearest_gp_path}')
    df_nearest_gp = df_nearest_gp[['lat', 'lon', 'pm25', 'hrrr_lat', 'hrrr_lon']].copy()
    df_nearest_gp = df_nearest_gp.rename(columns={'lat': 'pa_lat', 'lon': 'pa_lon'})

    return df_hrrr_data, df_nearest_gp, hrrr_metadata_dict, original_hrrr_cols


def calc_pa_average(pa_nearest_gp_data_df, method='median'):
    '''
    group all PA sensors that are nearest to the same hrrr grid point.
    method - the averaging method to aggregat the PA values, defult is median.
    '''
    nearest_grouped = pa_nearest_gp_data_df.groupby(by=['hrrr_lat','hrrr_lon'], sort=False, dropna=False, as_index=False)
    nearest_grouped = nearest_grouped.agg(
        mean_pa_lat=('pa_lat','mean'),
        mean_pa_lon=('pa_lon','mean'),
        averaged_pa_value=('pm25', method),
        pa_count=('pm25','size')
        )

    return nearest_grouped


def override_hrrr_with_pa_average(hrrr_data_df, pa_nearest_gp_grouped):
    '''
    overide all hrrr gp that have PA aggregated values
    '''

    merged = pd.merge(left=hrrr_data_df,
                      right=pa_nearest_gp_grouped,
                      how='left',
                      left_on=['lat','lon'],
                      right_on=['hrrr_lat','hrrr_lon'])

    merged.loc[merged.pa_count.isna(), 'pa_count'] = 0
    # replace pm2.5 value
    mask = merged.pa_count > 1
    replace_idx = mask[mask].index

    new_hrrr_df = merged.copy()
    new_hrrr_df.loc[replace_idx, 'hrrr_pm25'] = new_hrrr_df.loc[replace_idx, 'averaged_pa_value']
    new_hrrr_df = new_hrrr_df[['lat', 'lon', 'hrrr_pm25']]

    return new_hrrr_df, merged #merged for statistics


def prep_new_hrrr_to_json(new_hrrr_df, hrrr_metadata, original_hrrr_cols):
    new_hrrr_df = new_hrrr_df[['lat', 'lon', 'hrrr_pm25']]
    new_hrrr_df['pollutants'] = new_hrrr_df['hrrr_pm25'].apply(lambda x: {'pm25': x})
    new_hrrr_df = new_hrrr_df.drop(columns='hrrr_pm25')

    new_hrrr_df = new_hrrr_df[original_hrrr_cols]

    new_hrrr_list_of_dicts = new_hrrr_df.to_dict('records')

    new_hrrr_dict_full = {'values': new_hrrr_list_of_dicts,
                          'metadata': hrrr_metadata}

    return new_hrrr_dict_full


def visualize_stats(merged_df, hrrr_fn):

    # plot general distribution of affected gp
    merged_df['pa_count'] = merged_df['pa_count'].fillna(0)

    fig = plt.figure(figsize=(19,9))

    bins = list(range(int(merged_df['pa_count'].min()), int(merged_df['pa_count'].max())))

    plt.hist(merged_df['pa_count'], bins=bins, log=True)
    plt.title(f'HRRR grid points distribution by the number of affecting PA sensors\n {hrrr_fn}')
    plt.ylabel('# grid points (Note the log scale!)')
    plt.xlabel('number of PA sensors affecting an HRRR gridpoint')
    plt.grid()

    # plot distribution of affected gp by type of effect
    affected_gp = merged_df.copy().dropna().reset_index(drop=True)
    hrrr_higher_than_pa = affected_gp[affected_gp.hrrr_pm25 > affected_gp.averaged_pa_value].copy().reset_index(drop=True)
    hrrr_lower_than_pa = affected_gp[affected_gp.hrrr_pm25 < affected_gp.averaged_pa_value].copy().reset_index(drop=True)
    hrrr_equal_to_pa = affected_gp[affected_gp.hrrr_pm25 == affected_gp.averaged_pa_value].copy().reset_index(drop=True)

    affected_data = [hrrr_higher_than_pa.pa_count,
                     hrrr_equal_to_pa.pa_count,
                     hrrr_lower_than_pa.pa_count]

    labels = [f'hrrr > pa : {np.round(100*len(hrrr_higher_than_pa)/len(affected_gp),1)}% of affectd gp',
              f'hrrr == pa : {np.round(100*len(hrrr_equal_to_pa)/len(affected_gp),1)}% of affectd gp',
              f'hrrr < pa : {np.round(100*len(hrrr_lower_than_pa)/len(affected_gp),1)}% of affectd gp']

    fig = plt.figure(figsize=(19,9))

    bins = 20

    plt.hist(affected_data, bins=bins, align='mid', log=True, label=labels)
    plt.title('Affected HRRR grid points distribution by the number of affecting PA sensors\n groupd by type of change\n {hrrr_fn}')
    plt.ylabel('# *affected* grid points (Note the log scale!)')
    plt.xlabel('number of PA sensors affecting an HRRR gridpoint')
    plt.grid()
    plt.legend()
    return


def load_pickled_data(hr):
    print(f'{dt.utcnow()} - loading pickles...')
    f = f'hrrr_data_{hr}.pickle'
    hrrr_data = pd.read_pickle(f)
    f = f'pa_grouped_{hr}.pickle'
    pa_nearest_gp_grouped = pd.read_pickle(f)
    f = f'hrrr_grid_{hr}.pickle'
    grid_hrrr = pd.read_pickle(f)
    return hrrr_data, pa_nearest_gp_grouped, grid_hrrr

def bound_to_california(df):
    california_bbox = {'SW': {'lat': 32.385, 'lon': -124.545},
                       'NE': {'lat': 42.108, 'lon': -115.954}}
    result = df[(df.lat > california_bbox['SW']['lat']) &
                (df.lat < california_bbox['NE']['lat']) &
                (df.lon > california_bbox['SW']['lon']) &
                (df.lon < california_bbox['NE']['lon'])].copy().reset_index(drop=True)
    return result


if __name__=='__main__':
    """
    replace local paths below
    """
    data_folder_path = '## LOCAL PATH TO DATA HERE##'
    results_folder_path = '## LOCAL PATH TO OUTPUT FOLDER HERE##'

    test_files = {'hr1': ['2021-08-06T00_00_00_hrrr.json', '2021-08-06T00-00_pa_with_hrrr.pkl'],
                  'hr2': ['2021-08-06T12_00_00_hrrr.json', '2021-08-06T12-00_pa_with_hrrr.pkl']}

    for hr in list(test_files.keys()):
        hrrr_fn = f'{data_folder_path}{test_files[hr][0]}'
        nearest_gp_fn = f'{data_folder_path}{test_files[hr][1]}'
        print(f'starting {hr}')
        print(test_files[hr][0])
        print(f'{dt.utcnow()} - loading files...')
        hrrr_data, pa_nearest_gp_data, hrrr_metadata, original_hrrr_cols = load_files(hrrr_fn, nearest_gp_fn)

        print(f'{dt.utcnow()} - aggregate PA...')
        pa_nearest_gp_grouped = calc_pa_average(pa_nearest_gp_data, method='median')
        grid_hrrr = hrrr_data[['lat', 'lon']].copy()

        print(f'{dt.utcnow()} - the rest...')
        # grouping sensors
        pa_stations_groupd = pa_nearest_gp_grouped[pa_nearest_gp_grouped.pa_count>1].copy()
        pa_stations_groupd = pa_stations_groupd[['hrrr_lat', 'hrrr_lon', 'averaged_pa_value']]
        pa_stations_groupd = pa_stations_groupd.rename(columns={'hrrr_lat': 'lat',
                                                                'hrrr_lon': 'lon',
                                                                'averaged_pa_value': 'pm25'})

        max_dist_deg = 2.5 * 0.027    # 2.5 pixels
        max_dist_r = d2r(max_dist_deg)
        min_dist_r = d2r(0.027)  # one pixel

        # california bounded dataframes
        california_bbox = {'SW': {'lat': 32.385, 'lon': -124.545},
                           'NE': {'lat': 42.108, 'lon': -115.954}}

        california_hrrr = bound_to_california(grid_hrrr)
        california_pa = bound_to_california(pa_stations_groupd)
        california_hrrr_with_data = hrrr_data.copy().rename(columns={'hrrr_pm25': 'pm25'})
        california_hrrr_with_data = bound_to_california(california_hrrr_with_data)

        pa_nearest_gp_grouped.rename(columns={'hrrr_lat': 'lat', 'hrrr_lon': 'lon', 'averaged_pa_value': 'pm25'}, inplace=True)
        pa_grouped = pa_nearest_gp_grouped.copy()
        pa_raw = california_hrrr.merge(pa_grouped, on=['lat', 'lon'], how='left')
        pa_raw.loc[pa_raw.pa_count.isna(), 'pa_count'] = 0
        # replace pm2.5 value
        masking = pa_raw.pa_count > 1
        replace_idx = masking[masking].index
        pa_raw = pa_raw.loc[replace_idx]
        # pa_raw = california_hrrr.merge(pa_grouped, on=['lat', 'lon'], how='left')
        pa_raw = pa_raw[['lat', 'lon', 'pm25']]
        pa_raw.drop_duplicates(['lat', 'lon'], inplace=True, ignore_index=True)
        california_pa = bound_to_california(pa_raw)
        # IDW
        idw_pa_grouped = IDW(california_hrrr, california_pa, max_dist_r)
        # extend the dataframes on the grid to match all other dataframe sizes
        idw_pa = california_hrrr.merge(idw_pa_grouped, on=['lat', 'lon'], how='left')
        california_pa = california_hrrr.merge(california_pa, on=['lat', 'lon'], how='left')

        # copy HRRR with data
        hrrr = hrrr_data.copy()
        california_pa = california_pa.pivot_table(index='lat', columns='lon', values='pm25', dropna=False)
        idw_pivoted = idw_pa_grouped.pivot_table(index='lat', columns='lon', values='pm25', dropna=False)
        california_hrrr_with_data = california_hrrr_with_data.pivot_table(index='lat', columns='lon', values='pm25',
                                                                          dropna=False)

        # making a mask of interpolated PA sensors and dialating, then creating a fade out gradient
        mask = np.ones(idw_pivoted.size)
        mask[np.isnan(idw_pivoted.values.flatten())] = 0
        mask_rect = mask.reshape(idw_pivoted.shape)

        # dilation parameters
        dilatation_size = 1
        dilation_shape = 0
        element = cv2.getStructuringElement(dilation_shape, (2 * dilatation_size + 1, 2 * dilatation_size + 1),
                                           (dilatation_size, dilatation_size))
        dilated_mask_1 = cv2.dilate(mask_rect, element)
        dilated_mask_2 = cv2.dilate(dilated_mask_1, element)
        mask_diff1 = dilated_mask_1 - mask_rect
        mask_diff2 = dilated_mask_2 - dilated_mask_1
        # creating a mask with two-pixel gradient
        weighted_mask = mask_rect + 0.7*mask_diff1 + 0.3*mask_diff2

        extent = (california_bbox['SW']['lon'],
                  california_bbox['NE']['lon'],
                  california_bbox['SW']['lat'],
                  california_bbox['NE']['lat'])

        #converting the array to dataframe with the same lat lon indexes as the rest of the data
        weights = pd.DataFrame(data=weighted_mask,
                               index=idw_pivoted.index,
                               columns=idw_pivoted.columns)

        # unpivoting for smooshing purposes
        idw_pa = idw_pivoted.melt(value_name='pm25', ignore_index=False)
        idw_pa.reset_index(inplace=True)
        hrrr_m = california_hrrr_with_data.melt(value_name='pm25', ignore_index=False)
        hrrr_m.reset_index(inplace=True)
        weights = weights.melt(value_name='pm25', ignore_index=False)
        weights.reset_index(inplace=True)

        # smooshing the layers
        final = smoosh_frames(idw_pa, hrrr_m, weights)

        # visualize result:
        final_pivoted = final.pivot_table(index='lat', columns='lon', values='pm25', dropna=False)
        plt.figure()
        plt.imshow(final_pivoted, interpolation=None, origin='lower', extent=extent, cmap='gray', vmin=0, vmax=100)
        plt.colorbar()
        plt.savefig(f'output_figure_{hr}.png')
        final.rename(columns={'pm25': 'hrrr_pm25'}, inplace=True)

        # this next part is only relevant if the fix is partial (say california only)
        # removing california data from the bigger HRRR dataframe
        hrrr = hrrr[(hrrr.lat <= california_bbox['SW']['lat']) |
                 (hrrr.lat >= california_bbox['NE']['lat']) |
                 (hrrr.lon <= california_bbox['SW']['lon']) |
                 (hrrr.lon >= california_bbox['NE']['lon'])]
        # adding back updated california data
        hrrr = pd.concat([hrrr, final])
        # sorting coordinates
        hrrr.sort_values(['lat', 'lon'], inplace=True)
        # exporting the result in HRRR standard form
        new_hrrr_full = prep_new_hrrr_to_json(new_hrrr_df=hrrr, hrrr_metadata=hrrr_metadata, original_hrrr_cols=original_hrrr_cols)
        with open(f'{results_folder_path}FIXED_{test_files[hr][0]}', 'w') as fout:
            json.dump(new_hrrr_full, fout)

